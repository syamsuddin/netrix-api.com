<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Acm extends REST_Backend {

    public function __construct() {
        parent::__construct();
    }

    public function index_get($id = '') {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        $order = "id";
        // Execute
        $query = $this->zainapi->read('api_logs')
            ->column();
        if(!empty($this->get("date"))) {
            list($start, $end) = explode(' - ', $this->get('date'));
            $start = DATE("Y-m-d",strtotime($start));
            $end = DATE("Y-m-d",strtotime($end));
            $query = $query->where("request_dt > '" . $start . " 00:00:00' AND request_dt < '" . $end . " 23:59:59'");
        }
        if(!empty($search)) {
            $query = $query->where('cntrStatus != 0 AND (cntrName LIKE "%' . $search . '%")');
        } else {
            if(!empty($id)) {
                $query = $query->where(array('cntrId = ' . $id . ' AND cntrStatus !=' => 0));
            } else {
                // $query = $query->where(array('cntrStatus !=' => 0));
            }
        }
        if(!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if(!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if(empty($sort)) {
            $sort = 'desc';
        }
        if(!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        }
        $query = $query->render_pagination()->exe();
        $this->data = array(
          "data" => $query['data'],
          "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }
}
