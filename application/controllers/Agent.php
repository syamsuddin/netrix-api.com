<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Agent extends REST_Backend {

    public function __construct() {
        parent::__construct();
    }

	public function index_post() {
        $post = $this->input->post();


		if(empty($post["memberid"])){

            $this->data = array(
              "message"  => "member id not available"
            );

		}else{
			$qr = "declare @E_RES nvarchar(50)
						EXEC callMemberVerification @MEMBER_ID='{$post["memberid"]}', @EXEC_RESULT = @E_RES OUTPUT;
						PRINT @E_RES";

			$data = $this->zainapi->query_sp($qr);
			$data = explode(':',$data);
			$this->data = array(
				  "requestid" => $_SERVER['HTTP_REQUESTID'],
				  "ismember" => ($data[0]=='00')?TRUE:FALSE
			);

			$this->message = array(
				  "code" => $data[0],
				  "message" => $data[1]
			);

            $api_logs = array();
            $api_logs["api_id"] = 1;
            $api_logs["command_id"] = $this->commandid;
            $api_logs["request_id"] = $this->requestid;
            $api_logs["request_dt"] = $this->requestdt;
            $api_logs["client_id"] = $this->caller_id;
            $api_logs["client_name"] = $this->caller_name;
            $api_logs["request_url"] = $this->requesturl;
            $api_logs["signature"] = $this->signature;
            $api_logs["method"] = $_SERVER['REQUEST_METHOD'];

            $data = $this->zainapi->create('api_logs')
                ->data($api_logs)
                ->rule(array(
                    array(
                        'field' => 'api_id',
                        'label' => 'api ID',
                        'rules' => 'trim|required',
                    ),
                ))
                ->table_matching(array(
                    'api_id' => 'api_id',
                    'command_id' => 'command_id',
                    'request_id' => 'request_id',
                    'request_dt' => 'request_dt',
                    'client_id' => 'client_id',
                    'client_name' => 'client_name',
                    'request_url' => 'request_url',
                    'signature' => 'signature',
                    'method' => 'method',
                ))
                ->exe();


		}

        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

}
