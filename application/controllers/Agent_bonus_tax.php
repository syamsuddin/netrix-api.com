<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Agent_bonus_tax extends REST_Backend
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index_post()
    {
        $post = $this->input->post();
        if (empty($_FILES["attachmentfile"])) {
            $this->error = 1;
            $this->data = array(
              "isinsert" => false,
              "message"  => array(
              "file" => "FILE UPLOAD EMPTY"
              )
            );
        } else {
            $_FILES["attachmentfile"]["name"] = str_replace(" ","_",$_FILES["attachmentfile"]["name"]);
            $arr_type = array("text/csv", "application/vnd.ms-excel");
            if (!in_array($_FILES["attachmentfile"]["type"], $arr_type)) {
                $this->data = array(
                  "isinsert" => false,
                  "message"  => array(
                    "file" => "Format must csv"
                  )
                );
                $this->message = array(
                  "code" => "01",
                  "message" => "File should csv format"
                );
            } else {
                $query = $this->zainapi->read('t_file_tax')
                    ->column(array(
                        'ft_id' => 'id',
                        'insurance_id' => 'insurance_id',
                        'ft_file' => 'file_name',
                        'ft_save_dt' => 'save_time',
                        'ft_process' => 'status',
                    ))
                    ->where(array('ft_file LIKE' => '%' . $_FILES["attachmentfile"]["name"] . '%'))
                    ->exe();

                if(!empty($query['data'])){
                    $do_upload = FALSE;
                    $status = 2;
                }else{
                    $do_upload = TRUE;
                    $status = 1;
                }

                $directory = './uploads/tax/';
                $config['upload_path']          = $directory;
                $config['allowed_types']        = 'csv';
                $config['max_size']             = 15000;
                $config['file_name']            = $this->requestid . "_" . $_FILES["attachmentfile"]["name"];

                $this->load->library('upload', $config);

                if($do_upload){
                    if (!$this->upload->do_upload('attachmentfile')) {
                        $this->error = 1;
                        $this->data = array(
                          "isreceived" => false,
                          "message" => $this->message = $this->upload->display_errors()
                        );

                        return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
                    }
                }

                date_default_timezone_set("Asia/Jakarta");
                $datas = array(
                    "insurance_id" => $this->caller_id,
                    "file" => $config['file_name'],
                    "channel" => "WEB",
                    "save" => date("Y-m-d H:i:s"),
                    "process" => $status,
                );

                $data = $this->zainapi->create('t_file_tax')
                    ->data($datas)
                    ->rule(array(
                        array(
                            'field' => 'insurance_id',
                            'label' => 'insurance',
                            'rules' => 'trim|required',
                        ),
                    ))
                    ->table_matching(array(
                        'insurance_id' => 'insurance_id',
                        'ft_file' => 'file',
                        'ft_channel' => 'channel',
                        'ft_save_dt' => 'save',
                        'ft_process' => 'process',
                    ))
                    ->exe();
                if ($data['error'] == 1) {
                    $this->error = 1;
                    $this->data = array(
                      "isrecevied" => false,
                      "message"  => $data['message']
                    );
                    unlink($directory.$config['file_name']);
                } else {
                    $this->error = 0;
                    $this->data = array(
                      "requestid" => $_SERVER['HTTP_REQUESTID'],
                      "isrecevied" => true
                    );

                    $api_logs = array();
                    $api_logs["api_id"] = 4;
                    $api_logs["command_id"] = $this->commandid;
                    $api_logs["request_id"] = $this->requestid;
                    $api_logs["request_dt"] = $this->requestdt;
                    $api_logs["client_id"] = $this->caller_id;
                    $api_logs["client_name"] = $this->caller_name;
                    $api_logs["request_url"] = $this->requesturl;
                    $api_logs["signature"] = $this->signature;
                    $api_logs["filesend"] = $config['file_name'];
                    $api_logs["method"] = $_SERVER['REQUEST_METHOD'];

                    $data = $this->zainapi->create('api_logs')
                        ->data($api_logs)
                        ->rule(array(
                            array(
                                'field' => 'api_id',
                                'label' => 'api ID',
                                'rules' => 'trim|required',
                            ),
                        ))
                        ->table_matching(array(
                            'api_id' => 'api_id',
                            'command_id' => 'command_id',
                            'request_id' => 'request_id',
                            'request_dt' => 'request_dt',
                            'client_id' => 'client_id',
                            'client_name' => 'client_name',
                            'request_url' => 'request_url',
                            'signature' => 'signature',
                            'filesend' => 'filesend',
                            'method' => 'method',
                        ))
                        ->exe();
                }
            }
        }

        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }
}
