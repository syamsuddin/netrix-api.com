<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Auth extends REST_Backend {

    public function __construct() {
        parent::__construct();
    }

    public function index_post() {
        $post = $this->input->post();
        if (!empty($post['password'])) {
            $post['password'] = $this->zaincode->pass_hash($post['password']);
        }
        $auth = $this->zainapi->auth('user', array(
            array(
                'column' => 'userUsername',
                'value' => $post['username'],
            ),
            array(
                'column' => 'userPassword',
                'value' => $post['password'],
            ),
            array(
                'column' => 'userStatus',
                'value' => 1,
            ),
        ), 'userId');
        if ($auth !== false) {
            // Generate Token
            $token = $this->zaincode->generateRandomString(256);
            // Save Token To Database
            $data_token = array(
                'uuid' => $this->uuid,
                'token' => $token,
                'auth_id' => $auth,
            );

            $save_token = $this->zainapi->create('token')
                ->data($data_token)
                ->rule(array(
                    array(
                        'field' => 'uuid',
                        'label' => 'uuid',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'token',
                        'label' => 'token',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'auth_id',
                        'label' => 'auth id',
                        'rules' => 'trim|required',
                    ),

                ))
                ->table_matching(array(
                    'tkenUUID' => 'uuid',
                    'tkenHASH' => 'token',
                    'tkenAuthId' => 'auth_id',
                ))
                ->exe();

            $role = $this->zainapi->read('user')
                ->column(array(
                    'userRoleId' => 'role',
                    'userCallerId' => 'caller_id',
                ))
                ->where(array('userStatus !=' => 0, 'userId' => $auth))
                ->order('userUsername ASC')
                ->exe();

            $access = $this->zainapi->read('role_access')
                ->column(array(
                    'raccRoleId' => 'role_id',
                    'raccCntrId' => 'controllers_id',
                    'raccPOST' => 'post',
                    'cntrName' => 'controllers_name',
                ))
                ->join('controllers','controllers.cntrId = raccCntrId','LEFT')
                ->where(array('raccRoleId' => $role['data'][0]['role'], 'raccPOST' => 1))
                ->order('raccCntrId ASC')
                ->exe();

            $arr_access = array();
            foreach($access['data'] as $val){
                array_push($arr_access,$val['controllers_name']);
            }

            $caller = $this->zainapi->read('t_caller')
                ->column(array(
                    'caller_auth_header' => 'auth_name',
                    'caller_auth_password' => 'auth_password',
                    'caller_key_secret' => 'key_secret',
                ))
                ->where(array('caller_status' => "ACTIVE", 'caller_id' => $role['data'][0]['caller_id']))
                ->order('caller_name ASC')
                ->exe();

            // Return Token
            $this->data = array(
                'token' => $token,
                'role' => $role['data'][0]['role'],
                'auth_name' => $caller['data'][0]['auth_name'],
                'auth_password' => $caller['data'][0]['auth_password'],
                'key_secret' => $caller['data'][0]['key_secret'],
                'access' => $arr_access,
            );
            $this->auth_id = $role['data'][0]['role'];
        } else {
            $this->error = 1;
  			$this->data = array(
  			  "data" => array('message'=>'wrong password')
  			);
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function session_get() {
        $query = $this->zainapi->read('token')
			->column(array(
				'userUsername' => 'username',
				'userFullName' => 'fullname',
				'userRoleId' => 'role',
				'userPicture' => 'picture',
				'tkenHASH'=>'token',
				'tkenAuthId'=>'auth_id'
			))
			->where(array('tkenHASH' => $this->token))
			->where(array('tkenUUID' => $this->uuid))
			->join('user', 'tkenAuthId = userId', 'LEFT')
      ->exe();

		if(empty($query['data'])){
			 $this->data = array(
			  "data" => array('message'=>'not login')
			);
		}else{
			 $this->data = array(
			  "data" => $query['data'][0]
			);
        }
		$this->set_response($this->generate_result(), REST_Controller::HTTP_OK);

    }

	public function session_delete() {
         $query = $this->zainapi->delete('token')
            ->where(array('tkenAuthId' => $this->auth_id))
            ->exe();
        $this->data = $query['data'];
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function passhash_get($password = '') {
        $this->data = array(
            'hash' => $this->zaincode->pass_hash($password)
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

}
