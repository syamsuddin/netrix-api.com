<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Bonuscalc extends REST_Backend
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index_get($id = '')
    {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('t_gen')
            ->column()
            ->join('t_caller', 'caller_id = gen_insurance_id', 'LEFT')
            ->join('t_process', 'process_id = gen_process', 'LEFT');
            // ));
        if(!empty($this->get('caller'))){
            if($this->get('caller') != "ALL"){
                $query = $query->where('t_gen.gen_insurance_id = ' . $this->get('caller'));
            }
        }
        if(!empty($this->get('date'))){
            // $date = $this->get('date');
            // $query = $query->where("t_gen_summary.gen_period = '" . $date . "-01'");
            $period = str_replace("-","", $this->get('date'));
            $query = $query->where(array("t_gen.gen_period" => $period));
        }
        if (!empty($search)) {
            $query = $query->where('TASK_STATUS != 0 AND (GENERATE_ID LIKE "%' . $search . '%" OR PLAN_NAME LIKE "%' . $search . '%")');
        } else {
            if (!empty($id)) {
                $query = $query->where(array('gen_id' => $id));
            } else {
                $query = $query->where("gen_process < 17");
            }
        }
        if (!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if (!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if (empty($sort)) {
            $sort = 'asc';
        }
        if (!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('gen_id' . ' ' . strtoupper('desc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
            "data" => $query['data'],
            "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function listcommission_get($id = '')
    {
        $insurance = $this->get('bonusdist_insurance');
        $period = $this->get('start');
        $period = str_replace("-","", $period);

        $query = $this->zainapi->read('t_file_commission')
            ->column(array(
                'fc_id' => 'id',
                'insurance_id' => 'insurance',
                'fc_file' => 'file',
                'fc_process' => 'process',
                'fc_period' => 'period',
            ))
            ->where(array(
                'insurance_id' => $insurance,
                'fc_period' => $period,
                'fc_process' => 7,
            ));
		$query = $query->order('fc_id' . ' ' . strtoupper('desc'));
        $query = $query->exe();
        $this->data = array(
            "data" => $query['data'],
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function detail_get($id = '')
    {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('t_gen_plan')
             ->column();
			 //array(
             //   'gen_id' => 'id',
              //  'k_link_member_id' => 'k_link_member_id',
               // 'total_premium' => 'total_premium',
                //'total_bonus_personal' => 'total_bonus_personal',
                //'total_gross_commission' => 'total_gross_commission',
                //'sub_br' => 'sub_br',
                //'sub_bn' => 'sub_bn',
                //'sub_commission' => 'sub_commission',
                //'remark' => 'remark',
				//'remark' => 'remark',
				//'remark' => 'remark',
				//'//remark' => 'remark',
            //));
        if(!empty($this->get('caller'))){
            if($this->get('caller') != "ALL"){
                $query = $query->where('t_gen_summary.insurance_id = ' . $this->get('caller'));
            }
        }
        if(!empty($this->get('date'))){
            // $date = str_replace("-","",$this->get('date'));
            $date = $this->get('date');
            $query = $query->where("t_gen_summary.gen_period = '" . $date . "-01'");
        }
        if (!empty($search)) {
            $query = $query->where('TASK_STATUS != 0 AND (GENERATE_ID LIKE "%' . $search . '%" OR PLAN_NAME LIKE "%' . $search . '%")');
        } else {
            if (!empty($id)) {
                $query = $query->where(array('gen_id' => $id));
            } else {
                $query = $query->where("gen_process > 8 AND gen_process < 14");
            }
        }
        if (!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if (!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if (empty($sort)) {
            $sort = 'asc';
        }
        if (!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('gen_id' . ' ' . strtoupper('desc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
            "data" => $query['data'],
            "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function detailsummary_get($id = '')
    {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('t_gen_distribution')
            ->column();

		$query = $query->where(array('gen_id' => $id));

        if (!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if (!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if (empty($sort)) {
            $sort = 'asc';
        }
        if (!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('gen_id' . ' ' . strtoupper('desc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
            "data" => $query['data'],
            "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function process_get($id = '')
    {
        $qr = "declare @E_RES varchar(max) EXEC callBonusCalculation @GEN_ID={$id}, @EXEC_RESULT = @E_RES OUTPUT; PRINT @E_RES";
        $data = $this->zainapi->query_sp($qr);

		if(strpos($data, ":") === false) {
            $this->error = 1;
    		$this->data = array(
    		  "isinsert" => false,
    		  "message"  => array(
    			"Process" => $data
    		  )
    		);

            return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
        }

        list($code, $message) = explode(':', $data);

		if($code == "00"){

            $query_get = $this->zainapi->read('t_gen_summary')
                ->column(array(
                    'gen_id' => 'id',
                    'insurance_id' => 'insurance_id',
                    'caller_outbound' => 'caller_outbound',
                    'caller_key_secret' => 'caller_key_secret',
                    'caller_key_iv' => 'caller_key_iv',
                    'caller_key_bit' => 'caller_key_bit',
                ))
                ->join('t_caller','t_caller.caller_id = insurance_id', 'LEFT')
                ->where('gen_id = ' . $id)
                ->exe();

            if(empty($query_get['data'])){
                $this->error = 1;
        		$this->data = array(
        		  "isinsert" => false,
        		  "message"  => array(
        			"Process" => "gen_id not found on t_gen_summary"
        		  )
        		);

                return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
            }

            $query_cal_sum = $this->zainapi->read('t_cal_summary')
                ->column()
                ->where('gen_id = ' . $id)
                ->exe();

            if(empty($query_cal_sum['data'])){
                $this->error = 1;
        		$this->data = array(
        		  "isinsert" => false,
        		  "message"  => array(
        			"Process" => "gen_id not found on t_cal_summary"
        		  )
        		);

                return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
            }

            $dcs_db = $query_cal_sum['data'];
            $caller = $query_get['data'][0];

            $array_csv = array();
            foreach($dcs_db as $key=>$dcs){
                $array_csv[$key]['gen_id'] = $dcs['gen_id'];
                $array_csv[$key]['k_link_member_id'] = $dcs['k_link_member_id'];
                $array_csv[$key]['gross_income'] = $dcs['total_br'] + $dcs['total_bn_1'] + $dcs['total_bn_2'] + $dcs['total_bn_3'] + $dcs['total_bn_4'] + $dcs['total_bn_5']
                                             + $dcs['total_bn_6'] + $dcs['total_bn_7'] + $dcs['total_bn_8'] + $dcs['total_bn_9'] + $dcs['total_bonus_personal'];
            }

            $csv_text = array();
            $key_s = $caller['caller_key_secret'];
            $iv = $caller['caller_key_iv'];
            $bit_check = $caller['caller_key_bit'];
            foreach($array_csv as $key=>$val){
                $text = implode(";",$array_csv[$key]);
                $text_num =str_split($text,$bit_check);
            	$text_num = $bit_check-strlen($text_num[count($text_num)-1]);
            	for ($i=0;$i<$text_num; $i++) {$text = $text . chr($text_num);}
            	$hasil = openssl_encrypt($text,'des-ede3-cbc',$key_s,OPENSSL_RAW_DATA | OPENSSL_NO_PADDING, $iv);
            	$csv_text[$key] = base64_encode($hasil);
            }

            $fp = fopen("./uploads/temporary_calc/" . $id . ".csv", "w");
            foreach ($csv_text as $line){
                fputcsv($fp,explode(';',$line));
            }
            fclose($fp);

            $file = getcwd() . "\\uploads\\temporary_calc\\" . $id . ".csv";
            $ch      = curl_init($query_get['data'][0]['caller_outbound']);
            $options = array(
                CURLOPT_RETURNTRANSFER      => true,
                CURLOPT_HEADER              => false,
                CURLOPT_FOLLOWLOCATION      => false,
                CURLOPT_AUTOREFERER         => true,
                CURLOPT_CONNECTTIMEOUT      => 20,
                CURLOPT_TIMEOUT             => 20,
                CURLOPT_POST                => 1,
                CURLOPT_POSTFIELDS          => array(
                  'isreceived' => "true",
                  'attachmentfile' => new CurlFile($file, 'text/csv', $id . ".csv")
                ),
                CURLOPT_SSL_VERIFYHOST      => 0,
                CURLOPT_SSL_VERIFYPEER      => false,
                CURLOPT_VERBOSE             => 1,
                CURLOPT_HTTPHEADER          => array(
                   'responseid' => "Tax" . DATE("YmdHis"),
                   'responsedt' => DATE("YmdHis"),
                )
            );

            curl_setopt_array($ch, $options);
            $data_curl  = curl_exec($ch);
            $curl_errno = curl_errno($ch);
            $curl_error = curl_error($ch);

            // print_r($data_curl);
            // print_r($curl_errno);
            // print_r($curl_error);
            curl_close($ch);

            $this->error = 0;
			$this->data = array(
			  "isinsert" => true,
			);

            $post_update["id"] = $id;
            $post_update["status"] = 10;
            $data = $this->zainapi->update('t_gen_summary')
                ->data($post_update)
                ->rule(array(
                    array(
                        'field' => 'id',
                        'label' => 'ID',
                        'rules' => 'trim|required|numeric',
                    ),
                ))
                ->table_matching(array(
                    'gen_process' => 'status'
                ))
                ->where(array('gen_id' => $post_update['id']))
                ->exe();

		}else{
			$this->error = 1;
			$this->data = array(
			  "isinsert" => false,
			  "message"  => array(
				"code" => $code,
				"message" => $message
			  )
			);
		}

        return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function index_post()
    {
        $post = $this->input->post();

        if(empty($post["start"])){
            $this->data = array(
    		  "isinsert" => false,
    		  "message"  => array(
    			"Date" => "Please select date"
    		  )
    		);

            return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
        }

        if(empty($post["commission_file"])){
            $this->data = array(
    		  "isinsert" => false,
    		  "message"  => array(
    			"File" => "Please select file commisson"
    		  )
    		);

            return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
        }

        $month = str_replace("-","",$post["start"]);
        $gen_id = time();
		$insurance_id = $post["bonusdist_insurance"];
        $commission = join(",",$post["commission_file"]);

		// print_r($commission);exit();

		//JANGAN LUPA KOMEN!!!
        // $qr = "INSERT INTO [dbo].[t_gen_summary] (insurance_id, gen_period, total_bonus_personal, total_premium, total_gross_commission) SELECT '{$insurance_id}','{$month}',COUNT ( distinct fc_id ),SUM ( premium ),SUM ( gross_commission ) FROM dbo.t_file_commission_extraction";
        // $data = $this->db->query($qr);

        $qr = "declare @E_RES varchar(max) EXEC callBonusGenerate @INSURANCE_ID='{$insurance_id}', @GEN_PERIOD='{$month}', @FC_LIST='{$commission}', @EXEC_RESULT = @E_RES OUTPUT; PRINT @E_RES";
		$data = $this->zainapi->query_sp($qr);

        if(strpos($data, ":") === false) {
            $this->error = 1;
    		$this->data = array(
    		  "isinsert" => false,
    		  "message"  => array(
    			"Process" => $data
    		  )
    		);

            return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
        }

		list($code, $message) = explode(':', $data);

		if ($code == "00") {
			$this->error = 0;
			$this->data = array(
			  "isinsert" => true,
			);
		}else{
			$this->error = 1;
			$this->data = array(
			  "isinsert" => false,
			  "message"  => array(
				"code" => $code,
				"message" => $message
			  )
			);
		}

        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function tax_post()
    {
        $post = $this->input->post();
        $gen_id = $post['bonusdist_id_tax'];
        $tax_id = $post['bonusdist_tax_id_tax'];
        $period = $post['bonusdist_period'];
		$year = substr($period, 0, 4);
		$month = substr($period, 4, 2);

        $qr = "declare @E_RES varchar(max) EXEC callBonusBV @GEN_ID='{$gen_id}', @GEN_YEARS='{$year}', @GEN_MONTH='{$month}', @TAX_ID='{$tax_id}' @EXEC_RESULT = @E_RES OUTPUT; PRINT @E_RES";
        $data = $this->zainapi->query_sp($qr);

        if(strpos($data, ":") === false) {
            $this->error = 1;
    		$this->data = array(
    		  "isinsert" => false,
    		  "message"  => array(
    			"Process" => $data
    		  )
    		);

            return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
        }

        list($code, $message) = explode(':', $data);

		if ($code == "00") {
			$this->error = 0;
			$this->data = array(
			  "isinsert" => true,
			);
		}else{
			$this->error = 1;
			$this->data = array(
			  "isinsert" => false,
			  "message"  => array(
                  "code" => $code,
  				  "message" => $message
			  )
			);
		}

        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function index_delete($id = '')
    {
        $post = array();
        $post['status'] = 12;
        $post['id'] = $id;
        $data = $this->zainapi->update('t_gen_summary')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'id',
                    'label' => 'ID',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching(array(
                'gen_process' => 'status'
            ))
            ->where(array('gen_id' => $post['id']))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
			$this->data = array(
			  "isdelete" => false,
			  "message"  => $data['message']
			);
        } else {
            $this->error = 0;
			$this->data = array(
			  "isdelete" => true,
			);
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }
}
