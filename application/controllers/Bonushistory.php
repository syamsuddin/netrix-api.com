<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Bonushistory extends REST_Backend
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index_get($id = '')
    {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('t_gen_payment_details')
            ->column();
            // ));
        if(!empty($this->get('caller'))){
            if($this->get('caller') != "ALL"){
                $query = $query->where('t_gen_summary.insurance_id = ' . $this->get('caller'));
            }
        }
        if(!empty($this->get('date'))){
            // $date = str_replace("-","",$this->get('date'));
            $date = $this->get('date');
            $query = $query->where("t_gen_summary.gen_period = '" . $date . "-01'");
        }
        if (!empty($search)) {
            $query = $query->where('TASK_STATUS != 0 AND (GENERATE_ID LIKE "%' . $search . '%" OR PLAN_NAME LIKE "%' . $search . '%")');
        } else {
            if (!empty($id)) {
                $query = $query->where(array('gen_id' => $id));
            } else {
                $query = $query->where("gen_process = 17");
            }
        }
        if (!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if (!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if (empty($sort)) {
            $sort = 'asc';
        }
        if (!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('payment_period' . ' ' . strtoupper('desc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
            "data" => $query['data'],
            "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }
}
