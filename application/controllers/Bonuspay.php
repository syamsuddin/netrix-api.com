<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Bonuspay extends REST_Backend
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index_get($id = '')
    {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('t_gen_payment')
            ->column()
            ->join('t_process', 'process_id = gen_process', 'LEFT');
            // ));
        if(!empty($this->get('caller'))){
            if($this->get('caller') != "ALL"){
                $query = $query->where('t_gen_summary.insurance_id = ' . $this->get('caller'));
            }
        }
        if(!empty($this->get('date'))){
            // $date = str_replace("-","",$this->get('date'));
            $date = $this->get('date');
            $query = $query->where("t_gen_summary.gen_period = '" . $date . "-01'");
        }
        if (!empty($search)) {
            $query = $query->where('TASK_STATUS != 0 AND (GENERATE_ID LIKE "%' . $search . '%" OR PLAN_NAME LIKE "%' . $search . '%")');
        } else {
            if (!empty($id)) {
                $query = $query->where(array('gen_id' => $id));
            } else {
                $query = $query->where("gen_process = 17");
            }
        }
        if (!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if (!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if (empty($sort)) {
            $sort = 'asc';
        }
        if (!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('payment_id' . ' ' . strtoupper('desc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
            "data" => $query['data'],
            "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function listcalc_get()
    {
        $period = $this->get('start');
        $period = str_replace("-","", $period);

        $query = $this->zainapi->read('t_gen_summary')
            ->column()
            ->where(array(
                'gen_period' => $period,
                // 'gen_process' => 7,
            ))
            ->join('t_caller', 'caller_id = insurance_id', 'LEFT');
		$query = $query->order('gen_id' . ' ' . strtoupper('desc'));
        $query = $query->exe();
        $this->data = array(
            "data" => $query['data'],
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function index_post()
    {
        $post = $this->input->post();

        if(empty($post["start"])){
            $this->data = array(
    		  "isinsert" => false,
    		  "message"  => array(
    			"Period" => "Please select period"
    		  )
    		);

            return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
        }

        if(empty($post["tax"])){
            $this->data = array(
    		  "isinsert" => false,
    		  "message"  => array(
    			"Tax" => "Please select tax type"
    		  )
    		);

            return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
        }

        if(empty($post["calc_file"])){
            $this->data = array(
    		  "isinsert" => false,
    		  "message"  => array(
    			"File" => "Please select Calculation File"
    		  )
    		);

            return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
        }

        $period = str_replace("-","",$post["start"]);
        $tax = $post["tax"];
        $calc = join(",",$post["calc_file"]);

        $qr = "declare @E_RES varchar(max) EXEC callPayment @period='{$period}', @file_id='{$calc}', @EXEC_RESULT = @E_RES OUTPUT; PRINT @E_RES";
        $data = $this->zainapi->query_sp($qr);

        if(strpos($data, ":") === false) {
            $this->error = 1;
    		$this->data = array(
    		  "isinsert" => false,
    		  "message"  => array(
    			"Process" => $data
    		  )
    		);

            return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
        }

        list($code, $message) = explode(':', $data);

		if ($code == "00") {
			$this->error = 0;
			$this->data = array(
			  "isinsert" => true,
			);
		}else{
			$this->error = 1;
			$this->data = array(
			  "isinsert" => false,
			  "message"  => array(
                  "code" => $code,
  				  "message" => $message
			  )
			);
		}

        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function detail_get($id = '') {

        $table = 't_gen_payment_detail';

        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read($table)
            ->column();
		$query = $query->where('t_gen_payment_detail.payment_id = ' . $id);
        if(!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if(!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if(empty($sort)) {
            $sort = 'asc';
        }
        if(!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        }else{
            $query = $query->order('t_gen_payment_detail.k_link_member_id asc');
        }
        $query = $query->render_pagination()->exe();
        $this->data = array(
          "data" => $query['data'],
          "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }
}
