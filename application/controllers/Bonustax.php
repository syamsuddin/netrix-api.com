<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Bonustax extends REST_Backend
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index_get($id = '')
    {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('GEN_SUMMARY')
            ->column(array(
                'GENERATE_ID' => 'id',
                'PLAN_TYPE' => 'plan_type',
                'caller_name' => 'plan_name',
                'TASK_STATUS' => 'task_status',
                'GENERATE_MONTH' => 'generate_month',
                'TOTAL_PLAN' => 'total_plan',
                'TOTAL_ZREFERRER' => 'total_zreferrer',
                'TOTAL_PREMIUM' => 'total_premium',
                'TOTAL_COMMISSION' => 'total_commission',
                'TOTAL_SHARING' => 'total_sharing',
                'TASK_STATUS' => 'status',
                'caller_tax' => 'insu_type',
            ))->join('caller', 'caller_id = PLAN_TYPE', 'LEFT');
            // ));
        if(!empty($this->get('caller'))){
            if($this->get('caller') != "ALL"){
                $query = $query->where('GEN_SUMMARY.PLAN_TYPE = ' . $this->get('caller'));
            }
        }
        if(!empty($this->get('date'))){
            $date = str_replace("-","",$this->get('date'));
            $query = $query->where("GEN_SUMMARY.GENERATE_MONTH = '" . $date . "'");
        }
        if (!empty($search)) {
            $query = $query->where('TASK_STATUS != 0 AND (GENERATE_ID LIKE "%' . $search . '%" OR PLAN_NAME LIKE "%' . $search . '%")');
        } else {
            $query = $query->where("TASK_STATUS > '4' AND TASK_STATUS < '7' OR TASK_STATUS = '2'");
        }
        if (!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if (!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if (empty($sort)) {
            $sort = 'asc';
        }
        if (!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('GENERATE_ID' . ' ' . strtoupper('asc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
            "data" => $query['data'],
            "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function index_post()
    {
        $post = $this->input->post();
        if (empty($_FILES["bonustax_file"])) {
            $this->error = 1;
            $this->data = array(
            "isinsert" => false,
            "message"  => array(
              "file" => "FILE UPLOAD EMPTY"
            )
          );
        } else {
            $arr_type = array("text/csv", "application/vnd.ms-excel");
            if (!in_array($_FILES["bonustax_file"]["type"], $arr_type)) {
                $this->data = array(
                "isinsert" => false,
                "message"  => array(
                  "file" => "Format must csv"
                )
              );
                $this->message = array(
                "code" => "01",
                "message" => "File should csv format"
              );
            } else {
                $directory = './uploads/tax/';
                $config['upload_path']          = $directory;
                $config['allowed_types']        = 'csv';
                $config['max_size']             = 15000;
                $config['file_name']            = $post["bonustax_id"] . "_".$this->requestid.".csv";

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('bonustax_file')) {
                    $this->error = 1;
                    $this->data = array(
                    "isreceived" => false,
                    "message" => $this->message = $this->upload->display_errors()
                  );
                } else {
                    // error_log($id, 3, "../netrix-api.com/uploads/ebc/{$id}");
                    $qr = "UPDATE [dbo].[GEN_SUMMARY] SET TASK_STATUS = 5 WHERE GENERATE_ID='{$post["bonustax_id"]}'";
                    $data = $this->db->query($qr);

                    $this->error = 0;
                    $this->data = array(
                      "requestid" => "filetax" . DATE("YmdHis"),
                      "isreceived" => true,
                    );

                    $api_logs = array();
                    $api_logs["api_id"] = 4;
                    $api_logs["command_id"] = $this->commandid;
                    $api_logs["request_id"] = $this->requestid;
                    $api_logs["request_dt"] = $this->requestdt;
                    $api_logs["client_id"] = $this->caller_id;
                    $api_logs["client_name"] = $this->caller_name;
                    $api_logs["request_url"] = $this->requesturl;
                    $api_logs["signature"] = $this->signature;
                    $api_logs["filesend"] = $config['file_name'];
                    $api_logs["method"] = $_SERVER['REQUEST_METHOD'];

                    $data = $this->zainapi->create('api_logs')
                        ->data($api_logs)
                        ->rule(array(
                            array(
                                'field' => 'api_id',
                                'label' => 'api ID',
                                'rules' => 'trim|required',
                            ),
                        ))
                        ->table_matching(array(
                            'api_id' => 'api_id',
                            'command_id' => 'command_id',
                            'request_id' => 'request_id',
                            'request_dt' => 'request_dt',
                            'client_id' => 'client_id',
                            'client_name' => 'client_name',
                            'request_url' => 'request_url',
                            'signature' => 'signature',
                            'filesend' => 'filesend',
                            'method' => 'method',
                        ))
                        ->exe();
                }
            }
        }

        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function index_delete($id = '')
    {
        $post = array();
        $post['status'] = 0;
        $post['role_id'] = $id;
        $data = $this->zainapi->update('role')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'role_id',
                    'label' => 'role id',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching(array(
                //'roleId' => 'role_id',
                'roleStatus' => 'status'
            ))
            ->where(array('roleId' => $post['role_id'], 'roleStatus !=' => 0))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->message = $data['message'];
        } else {
            $this->error = 0;
            $this->message = 'data deleted';
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }
}
