<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Bulk extends REST_Backend {

    public function __construct() {
        parent::__construct();
    }

    public function index_get($id = '') {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('PLAN')
            ->column(array(
                'PLAN_ID' => 'id',
                'PLAN_TYPE' => 'plan_type',
                'PLAN_NAME' => 'plan_name',
                'TASK_STATUS' => 'task_status',
                'PLAN_CHANNEL' => 'plan_channel',
                'TASK_TYPE' => 'task_type',
                'TASK_START_DT' => 'task_start_date',
                'TASK_END_DT' => 'task_end_date'
            ));
        //->join('role', 'roleId = userRoleId', 'LEFT');

    		if(!empty($this->get("plan_name_filter"))){
    			$query = $query->where("PLAN_NAME LIKE '%" . $this->get("plan_name_filter") . "%'");
    		}

    		if(!empty($this->get("plan_type_filter"))){
    			$query = $query->where("PLAN_TYPE = '" . $this->get("plan_type_filter") . "'");
    		}

    		if(!empty($this->get("created_by_filter"))){
    			$query = $query->where("PLAN_NAME LIKE '%" . $this->get("created_by_filter") . "%'");
    		}

    		if(!empty($this->get("plan_channel_filter"))){
    			$query = $query->where("PLAN_CHANNEL = '" . $this->get("plan_channel_filter") . "'");
    		}

    		if(!empty($this->get("status_filter"))){
    			$query = $query->where("TASK_STATUS = '" . $this->get("status_filter") . "'");
    		}

        if(!empty($search)) {
            $query = $query->where('userStatus != 0 AND (userUsername LIKE "%' . $search . '%" OR userFullName LIKE "%' . $search . '%")');
        } else {
            if(!empty($id)) {
                $query = $query->where(array('PLAN_ID = ' => $id));
            } else {
                //$query = $query->where(array('userStatus !=' => 0));
            }
        }
        if(!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if(!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if(empty($sort)) {
            $sort = 'asc';
        }
        if(!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        }
        $query = $query->render_pagination()->exe();
        $this->data = array(
          "data" => $query['data'],
          "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function index_post() {
        $post = $this->input->post();
        if(!empty($post['password'])) {
            $post['password'] = $this->zaincode->pass_hash($post['password']);
        }
        $data = $this->zainapi->create('user')
            ->data($post)
            ->unique('user', 'username', 'userUsername', 'userStatus != 0')
            ->rule(array(
                array(
                    'field' => 'username',
                    'label' => 'username',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'password',
                    'label' => 'password',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'fullname',
                    'label' => 'fullname',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'phone',
                    'label' => 'phone',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'role_id',
                    'label' => 'role id',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching(array(
                'userUsername' => 'username',
                'userPassword' => 'password',
                'userFullName' => 'fullname',
                'userPhone' => 'phone',
                'userRoleId' => 'role_id',
            ))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->message = $data['message'];
        } else {
            $this->error = 0;
            $this->message = 'data saved';
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function update_post() {
        $post = $this->input->post();
        $data = $this->zainapi->update('user')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'username',
                    'label' => 'username',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'fullname',
                    'label' => 'fullname',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'phone',
                    'label' => 'phone',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'role_id',
                    'label' => 'role id',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching(array(
                'userUsername' => 'username',
                'userFullName' => 'fullname',
                'userPhone' => 'phone',
                'userRoleId' => 'role_id',
            ))
            ->where(array('userId' => $post['user_id'], 'userStatus !=' => 0))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->message = $data['message'];
        } else {
            $this->error = 0;
            $this->message = 'data updated';
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function index_delete($id = '') {
		$post = array();
        $post['status'] = 0;
        $post['role_id'] = $id;
        $data = $this->zainapi->update('role')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'role_id',
                    'label' => 'role id',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching(array(
                //'roleId' => 'role_id',
                'roleStatus' => 'status'
            ))
            ->where(array('roleId' => $post['role_id'], 'roleStatus !=' => 0))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->message = $data['message'];
        } else {
            $this->error = 0;
            $this->message = 'data deleted';
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
	}

}
