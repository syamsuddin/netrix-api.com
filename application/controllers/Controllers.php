<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Controllers extends REST_Backend {

    public function __construct() {
        parent::__construct();
    }

    public function index_get($id = '') {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('controllers')
            ->column(array(
                'cntrId' => 'id',
                'cntrName' => 'name',
                'cntrStatus' => 'status',
            ));
        if(!empty($this->get("name"))) {
            $query = $query->where("cntrName LIKE '%" . $this->get("name") . "%'");
        }
        if(!empty($search)) {
            $query = $query->where('cntrStatus != 0 AND (cntrName LIKE "%' . $search . '%")');
        } else {
            if(!empty($id)) {
                $query = $query->where(array('cntrId = ' . $id . ' AND cntrStatus !=' => 0));
            } else {
                $query = $query->where(array('cntrStatus !=' => 0));
            }
        }
        if(!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if(!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if(empty($sort)) {
            $sort = 'asc';
        }
        if(!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('cntrId' . ' ' . strtoupper('asc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
          "data" => $query['data'],
          "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function index_post() {
        $post = $this->input->post();
        $data = $this->zainapi->create('controllers')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'controllers_name',
                    'label' => 'controllers name',
                    'rules' => 'trim|required',
                ),
            ))
            ->table_matching(array(
                'cntrName' => 'controllers_name',
            ))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isinsert" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isinsert" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function update_post() {
        $post = $this->input->post();
        $data = $this->zainapi->update('controllers')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'controllers_name',
                    'label' => 'controllers name',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'controllers_id',
                    'label' => 'controllers id',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching(array(
                'cntrName' => 'controllers_name',
            ))
            ->where(array('cntrId' => $post['controllers_id'], 'cntrStatus !=' => 0))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isupdate" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isupdate" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function index_delete($id = '') {
		$post = array();
        $post['status'] = 0;
        $post['cntr_id'] = $id;
        $data = $this->zainapi->update('controllers')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'cntr_id',
                    'label' => 'cntr id',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching(array(
                //'cntrId' => 'cntr_id',
                'cntrStatus' => 'status'
            ))
            ->where(array('cntrId' => $post['cntr_id'], 'cntrStatus !=' => 0))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isdelete" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isdelete" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
	}

}
