<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Encrypt extends REST_Backend {

    public function __construct() {
        parent::__construct();
    }

    public function index_post() {
        $post = $this->input->post();

        if(empty($_FILES)){
            $this->error = 0;
            $this->data = array(
              "isfinish" => false,
              "message" => array(
                  "File" => "Please select file"
              )
            );

            return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
        }

        $query = $this->zainapi->read('t_caller')
            ->column(array(
                'caller_id' => 'id',
                'caller_name' => 'name',
                'caller_auth_header' => 'auth_header',
                'caller_auth_password' => 'auth_password',
                'caller_key_secret' => 'secret_key',
                'caller_key_iv' => 'iv',
                'caller_key_bit' => 'bit',
                'caller_outbound' => 'outbound',
                'caller_tax' => 'tax',
                'caller_status' => 'status',
            ));
        $query = $query->where('caller_id = ' . $post['caller_id'])->exe();

        $caller = $query['data'][0];

        $key_s = $caller['secret_key'];
        $iv = $caller['iv'];
        $bit_check = $caller['bit'];


        $csv_text = array();
        $file_lines = file($_FILES['file_upload']['tmp_name']);

        if($post['type'] == 'encrypt'){
            foreach ($file_lines as $key=>$text) {
                $text = trim($text);
                $text_num =str_split($text,$bit_check);
            	$text_num = $bit_check-strlen($text_num[count($text_num)-1]);
            	for ($i=0;$i<$text_num; $i++) {$text = $text . chr($text_num);}
            	$hasil = openssl_encrypt($text,'des-ede3-cbc',$key_s,OPENSSL_RAW_DATA | OPENSSL_NO_PADDING, $iv);
            	$csv_text[$key] = base64_encode($hasil);
            }
            $error_descrypt = 0;
        }else{
            foreach ($file_lines as $key=>$text) {
                $text = trim($text);
                $decrypted = openssl_decrypt(base64_decode($text),'des-ede3-cbc',$key_s,OPENSSL_RAW_DATA | OPENSSL_NO_PADDING, $iv);
                $last_char=substr($decrypted,-1);
            	for($i=0;$i<$bit_check-1; $i++){
            		if(chr($i)==$last_char){
            			$decrypted=substr($decrypted,0,strlen($decrypted)-$i);
            			break;
            		}
            	}
                $replaced = preg_replace('/[\x00-\x1F\x7F]/', '', $decrypted);
            	$csv_text[$key] = trim($replaced);
                if(preg_match('/[^\x20-\x7e]/', $csv_text[$key])){
                    $error_descrypt = 1;
                }else{
                    $error_descrypt = 0;
                }
            }
        }

        if($error_descrypt == 1){
            $this->error = 1;
            $this->data = array(
              "isfinish" => false,
              "message" => array(
                  "Failed Descrypt" => "Check the insurance"
              )
            );
        }else{
            $this->error = 0;
            $this->data = array(
              "isfinish" => true,
              "data" => $csv_text
            );
        }


        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }
}
