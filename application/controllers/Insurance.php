<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Insurance extends REST_Backend {

    public function __construct() {
        parent::__construct();
    }

    public function index_get($id = '') {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('t_caller')
            ->column(array(
                'caller_id' => 'id',
                'caller_name' => 'name',
                'caller_outbound' => 'outbound',
                'caller_tax' => 'tax',
                'caller_type' => 'type',
            ));
        $query = $query->where("caller_type = 1");
		$query = $query->order('caller_name' . ' ' . strtoupper('asc'));
        $query = $query->exe();
        $this->data = array(
          "data" => $query['data'],
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function list_get($id = '') {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('t_caller')
            ->column(array(
                'caller_id' => 'id',
                'caller_name' => 'name',
                'caller_auth_header' => 'auth_header',
                'caller_auth_password' => 'auth_password',
                'caller_key_secret' => 'secret_key',
                'caller_key_iv' => 'iv',
                'caller_key_bit' => 'bit',
                'caller_outbound' => 'outbound',
                'caller_tax' => 'tax',
                'caller_type' => 'type',
                'caller_status' => 'status',
            ));
        if(!empty($this->get('name'))){
            $query = $query->where("caller_name LIKE '%" . $this->get('name') . "%'");
        }
        if(!empty($this->get('header'))){
            $query = $query->where("caller_auth_header LIKE '%" . $this->get('header') . "%'");
        }
        if(!empty($this->get('password'))){
            $query = $query->where("caller_auth_password LIKE '%" . $this->get('password') . "%'");
        }
        if(!empty($this->get('key_secret'))){
            $query = $query->where("caller_key_secret LIKE '%" . $this->get('key_secret') . "%'");
        }
        if(!empty($this->get('iv'))){
            $query = $query->where("caller_key_iv LIKE '%" . $this->get('iv') . "%'");
        }
        if(!empty($this->get('bit'))){
            $query = $query->where("caller_key_bit LIKE '%" . $this->get('bit') . "%'");
        }
        if(!empty($this->get('outbound'))){
            $query = $query->where("caller_outbound LIKE '%" . $this->get('outbound') . "%'");
        }
        if(!empty($this->get('tax'))){
            if($this->get('tax') != "ALL"){
                $query = $query->where("caller_tax = '" . $this->get('tax') . "'");
            }
        }
        if(!empty($this->get('type'))){
            if($this->get('type') != "ALL"){
                $query = $query->where("caller_type = '" . $this->get('type') . "'");
            }
        }
        if(!empty($this->get('status'))){
            if($this->get('status') != "ALL"){
                $query = $query->where("caller_status = '" . $this->get('status') . "'");
            }
        }
        if(!empty($search)) {
            $query = $query->where("caller_name LIKE '%" . $search . "%'");
        } else {
            if(!empty($id)) {
                $query = $query->where('caller_id = ' . $id);
            }else{
                $query = $query->where(array("caller_status" => "ACTIVE"));
            }
        }
        if(!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if(!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if(empty($sort)) {
            $sort = 'asc';
        }
        if(!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('caller_id' . ' ' . strtoupper('asc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
          "data" => $query['data'],
          "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function access_get($id_caller = '') {

        $query = $this->zainapi->read('t_api')
            ->column(array(
                'api.api_id' => 'id',
                'api_name' => 'name',
                'api_method' => 'method',
                'api_status' => 'status',
            ))
            ->where('api.api_status = \'ACTIVE\'')
            ->exe();

        foreach($query["data"] as $key=>$val){
            $data = $this->zainapi->read('t_caller_api_access')
                ->column()
                ->where('caller_id = ' . $id_caller . ' AND api_id = ' . $val["id"])
                ->exe();

            $res = $data["data"];
            if(!empty($res)){
                $query["data"][$key]["access"] = $res[0]["api_id"];
            }
        }

        $this->data = array(
          "data" => $query['data'],
        );

        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function access_post() {
        $post = $this->input->post();
        foreach($post["api_id"] as $val){
            $query = $this->zainapi->query('SELECT * FROM t_caller_api_access where caller_id = ' . $post["caller_id"] . ' AND api_id = ' . $val);
            if(isset($post["post"][$val])){
                if(empty($query)){
                    $this->db->query('insert into t_caller_api_access (caller_id, api_id,system_creation_dt,system_creation_name) values (' . $post["caller_id"] . ',' . $val . ',\'' . DATE("Y-m-d H:i:s") . '\',\'' . $post["caller_name"] . '\')');
                }
            }else{
                if(!empty($query)){
                    $this->db->query('DELETE FROM t_caller_api_access where caller_id = ' . $post["caller_id"] . ' AND api_id = ' . $val);
                }
            }
        }
        $this->data = array(
          "isupdate" => true,
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function index_post() {
        $post = $this->input->post();
        // if(!empty($post['insurance_password'])) {
        //     $post['insurance_password'] = $this->zaincode->pass_hash($post['insurance_password']);
        // }
        $post["insurance_status"] = "ACTIVE";
        $data = $this->zainapi->create('t_caller')
            ->data($post)
            ->unique('t_caller', 'insurance_username', 'caller_name', 'caller_status = \'ACTIVE\'')
            ->rule(array(
                array(
                    'field' => 'insurance_name',
                    'label' => 'name',
                    'rules' => 'trim|required',
                ),
				        array(
                    'field' => 'insurance_username',
                    'label' => 'username',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'insurance_password',
                    'label' => 'password',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'insurance_secret_key',
                    'label' => 'secret key',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'insurance_outbound',
                    'label' => 'URL',
                    'rules' => 'trim|required',
                ),
            ))
            ->table_matching(array(
                'caller_name' => 'insurance_name',
                'caller_auth_header' => 'insurance_username',
                'caller_auth_password' => 'insurance_password',
                'caller_key_secret' => 'insurance_secret_key',
                'caller_key_iv' => 'insurance_iv',
                'caller_key_bit' => 'insurance_bit',
                'caller_outbound' => 'insurance_outbound',
                'caller_tax' => 'insurance_tax',
                'caller_type' => 'insurance_type',
                'caller_status' => 'insurance_status',
            ))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isinsert" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isinsert" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function update_post() {
        $post = $this->input->post();
        $data = $this->zainapi->update('t_caller')
            ->data($post)
            ->unique('t_caller', 'insurance_username_edit', 'caller_name', "caller_status = 'ACTIVE' AND caller_id != '" . $post['insurance_id_edit'] . "'")
            ->rule(array(
                array(
                    'field' => 'insurance_name_edit',
                    'label' => 'name',
                    'rules' => 'trim|required',
                ),
				array(
                    'field' => 'insurance_username_edit',
                    'label' => 'username',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'insurance_password_edit',
                    'label' => 'password',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'insurance_outbound_edit',
                    'label' => 'URL',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'insurance_id_edit',
                    'label' => 'ID',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching(array(
                'caller_name' => 'insurance_name_edit',
                'caller_auth_header' => 'insurance_username_edit',
                'caller_auth_password' => 'insurance_password_edit',
                'caller_key_secret' => 'insurance_secret_key_edit',
                'caller_key_iv' => 'insurance_iv_edit',
                'caller_key_bit' => 'insurance_bit_edit',
                'caller_outbound' => 'insurance_outbound_edit',
                'caller_tax' => 'insurance_tax_edit',
                'caller_type' => 'insurance_type_edit',
            ))
            ->where(array("caller_id" => $post['insurance_id_edit']))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isupdate" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isupdate" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function index_delete($id = '') {
		$post = array();
        $post['status'] = "DEACTIVE";
        $post['insurance_id'] = $id;
        $data = $this->zainapi->update('t_caller')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'insurance_id',
                    'label' => 'Insurance ID',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching(array(
                'caller_status' => 'status'
            ))
            ->where(array("caller_id" => $post['insurance_id']))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isdelete" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isdelete" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
	}

  public function update_password_post() {
        $post = $this->input->post();
        if(!empty($post['insurance_password_edit'])) {
            $post['insurance_password_edit'] = $this->zaincode->pass_hash($post['insurance_password_edit']);
        }
        if(!empty($post['insurance_password_confirm_edit'])) {
            $post['insurance_password_confirm_edit'] = $this->zaincode->pass_hash($post['insurance_password_confirm_edit']);
        }
        $data = $this->zainapi->update('t_caller')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'insurance_password_edit',
                    'label' => 'password',
                    'rules' => 'trim|required',
                ),
  			        array(
                    'field' => 'insurance_password_confirm_edit',
                    'label' => 'confirm password',
                    'rules' => 'trim|required|matches[insurance_password_edit]',
                ),
            ))
            ->table_matching(array(
                'caller_auth_password' => 'insurance_password_edit',
            ))
            ->where('caller_id = ' . $post['insurance_id_edit'])
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isupdate" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isupdate" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

}
