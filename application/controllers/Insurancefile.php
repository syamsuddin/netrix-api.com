<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Insurancefile extends REST_Backend
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index_get($id = '')
    {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('t_file_commission')
            ->column(array(
                'fc_id' => 'id',
                'insurance_id' => 'insurance_id',
                'fc_file' => 'file_name',
                'fc_period' => 'period',
                'fc_channel' => 'channel',
                'fc_save_dt' => 'save_time',
                'fc_process' => 'status',
                'caller_name' => 'insurance_name',
                'process_name' => 'process_name',
            ))
        ->join('t_caller', 't_caller.caller_id = insurance_id', 'LEFT')
        ->join('t_process', 't_process.process_id = fc_process', 'LEFT')
        ->where('fc_process != 8');
        if(!empty($this->get('caller'))){
            if($this->get('caller') != "ALL"){
                $query = $query->where('t_file_commission.insurance_id = ' . $this->get('caller'));
            }
        }
		if(!empty($this->get('file_upload'))){
			$query = $query->where("t_file_commission.fc_file LIKE '%" . $this->get('file_upload') . "%'");
        }
        if(!empty($this->get('date'))){
            // list($start, $end) = explode(' - ', $this->get('date'));
            // $start = DATE("Y-m-d",strtotime($start));
            // $end = DATE("Y-m-d",strtotime($end));
            // $query = $query->where("fc_save_dt > '" . $start . " 00:00:00' AND fc_save_dt < '" . $end . " 23:59:59'");
            $period = str_replace("-","", $this->get('date'));
            $query = $query->where(array("fc_period" => $period));
        }
        if (!empty($search)) {
            $query = $query->where('fc_process != 0 AND (fc_id LIKE "%' . $search . '%" OR fc_file LIKE "%' . $search . '%")');
        } else {
            if (!empty($id)) {
                $query = $query->where(array('fc_id' => $id, 'fc_process !=' => 0));
            } else {
                $query = $query->where(array('fc_process !=' => 0));
            }
        }
        if (!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if (!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if (empty($sort)) {
            $sort = 'asc';
        }
        if (!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('fc_save_dt' . ' ' . strtoupper('desc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
          "data" => $query['data'],
          "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function detailall_get($id = '')
    {
        $type = $this->get('type');
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');

        if(empty($type)){
            $type = 1;
        }

        if($type == 1){
            $this->db->select("*");
            $this->db->from("t_file_commission_extraction");
            $this->db->where("fc_id",$id);
            $query1 = $this->db->get_compiled_select(); // It resets the query just like a get()

            $this->db->select("*");
            $this->db->from("t_file_commission_exception");
            $this->db->where("fc_id",$id);
            $query2 = $this->db->get_compiled_select();

            $query = $this->zainapi->read("( " . $query1." UNION ALL ".$query2 . " ) as tem")->column();
        }else if($type == 2){
            $query = $this->zainapi->read('t_file_commission_extraction')->column();
            $query = $query->where(array("fc_id" => $id));
        }else if($type == 3){
            $query = $this->zainapi->read('t_file_commission_exception')->column();
            $query = $query->where(array("fc_id" => $id));
        }

        if (!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if (!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if (empty($sort)) {
            $sort = 'asc';
        }
        if (!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('fc_id' . ' ' . strtoupper('desc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
          "data" => $query['data'],
          "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function sum_get($id = '', $type = '')
    {
        if($type == 0){
            $table = 't_file_commission_extraction';
        }else{
            $table = 't_file_commission_exception';
        }

        // Execute
        $query = $this->zainapi->read($table)
            ->column(array(
                'COUNT(fc_id)' => 'total',
            ))
            ->where(array('fc_id' => $id));
        $query = $query->exe();
        $this->data = array(
          "data" => $query['data']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function sp_get($id = '')
    {
        $this->zainapi->query_sp("BEGIN TRAN");
        $sp = $this->zainapi->query_sp("DECLARE @EXEC_RESULT nvarchar(max) EXEC callBonusExtraction @fc_id ={$id}, @EXEC_RESULT=@EXEC_RESULT OUTPUT; PRINT @EXEC_RESULT");
        list($code, $message) = explode(':', $sp);
        if($code == '00'){
            $this->zainapi->query_sp("COMMIT");
            // $query = $this->db->where('fc_id', $id)
			// ->update('t_file_commission', array('fc_process' => 7, 'fc_process_error_id' => $code, 'fc_process_error_message' => $message));
            $this->data = array(
                "issuccess" => true,
                "message"  => array(
                    "code" => $code,
                    "message" => $message
                )
            );
        }else{
            $this->zainapi->query_sp("ROLLBACK");
			$query = $this->db->where('fc_id', $id)
			->update('t_file_commission', array('fc_process' => 6, 'fc_process_error_id' => $code, 'fc_process_error_message' => $message));
            $this->data = array(
                "issuccess" => false,
                "message"  => array(
                    "code" => $code,
                    "message" => $message
                )
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function index_post()
    {
        $post = $this->input->post();
        if (empty($_FILES["insurancefile_file"])) {
            $this->error = 1;
            $this->data = array(
              "isinsert" => false,
              "message"  => array(
              "file" => "FILE UPLOAD EMPTY"
              )
            );
        } else {
            $_FILES["insurancefile_file"]["name"] = str_replace(" ","_",$_FILES["insurancefile_file"]["name"]);
            $_FILES["insurancefile_file"]["name"] = str_replace("[","",$_FILES["insurancefile_file"]["name"]);
            $_FILES["insurancefile_file"]["name"] = str_replace("]","",$_FILES["insurancefile_file"]["name"]);
            $arr_type = array("text/csv", "application/vnd.ms-excel");
            if (!in_array($_FILES["insurancefile_file"]["type"], $arr_type)) {
                $this->data = array(
                  "isinsert" => false,
                  "message"  => array(
                    "file" => "Format must csv"
                  )
                );
                $this->message = array(
                  "code" => "01",
                  "message" => "File should csv format"
                );
            } else {
                $query = $this->zainapi->read('t_file_commission')
                    ->column(array(
                        'fc_id' => 'id',
                        'insurance_id' => 'insurance_id',
                        'fc_file' => 'file_name',
                        'fc_save_dt' => 'save_time',
                        'fc_process' => 'status',
                    ))
                    ->where(array('fc_file LIKE' => '%' . $_FILES["insurancefile_file"]["name"] . '%'))
                    ->exe();

                if(!empty($query['data'])){
                    $do_upload = FALSE;
                    $status = 2;
                }else{
                    $do_upload = TRUE;
                    $status = 1;
                }

                $directory = './uploads/temporary/';
                $config['upload_path']          = $directory;
                $config['allowed_types']        = 'csv';
                $config['max_size']             = 15000;
                $config['file_name']            = $this->requestid . "_" . $_FILES["insurancefile_file"]["name"];

                $this->load->library('upload', $config);

                if($do_upload){
                    if (!$this->upload->do_upload('insurancefile_file')) {
                        $this->error = 1;
                        $this->data = array(
                          "isreceived" => false,
                          "message" => $this->message = $this->upload->display_errors()
                        );

                        return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
                    }
                }

                date_default_timezone_set("Asia/Jakarta");
                $post['insurancefile_period'] = str_replace("-","", $post['insurancefile_period']);
                $datas = array(
                    "insurance_id" => $post["insurancefile_insurance"],
                    "file" => $config['file_name'],
                    "period" => $post['insurancefile_period'],
                    "channel" => "WEB",
                    "save" => date("Y-m-d H:i:s"),
                    "process" => $status,
                );

                $data = $this->zainapi->create('t_file_commission')
                    ->data($datas)
                    ->rule(array(
                        array(
                            'field' => 'insurance_id',
                            'label' => 'insurance',
                            'rules' => 'trim|required',
                        ),
                        array(
                            'field' => 'period',
                            'label' => 'period',
                            'rules' => 'trim|required',
                        ),
                    ))
                    ->table_matching(array(
                        'insurance_id' => 'insurance_id',
                        'fc_file' => 'file',
                        'fc_period' => 'period',
                        'fc_channel' => 'channel',
                        'fc_save_dt' => 'save',
                        'fc_process' => 'process',
                    ))
                    ->exe();
                if ($data['error'] == 1) {
                    $this->error = 1;
                    $this->data = array(
                      "isrecevied" => false,
                      "message"  => $data['message']
                    );
                    unlink($directory.$config['file_name']);
                } else {
                    $this->error = 0;
                    $this->data = array(
                      "requestid" => $_SERVER['HTTP_REQUESTID'],
                      "isrecevied" => true
                    );

                    $api_logs = array();
                    $api_logs["api_id"] = 2;
                    $api_logs["command_id"] = $this->commandid;
                    $api_logs["request_id"] = $this->requestid;
                    $api_logs["request_dt"] = $this->requestdt;
                    $api_logs["client_id"] = $this->caller_id;
                    $api_logs["client_name"] = $this->caller_name;
                    $api_logs["request_url"] = $this->requesturl;
                    $api_logs["signature"] = $this->signature;
                    $api_logs["filesend"] = $config['file_name'];
                    $api_logs["method"] = $_SERVER['REQUEST_METHOD'];

                    $data = $this->zainapi->create('api_logs')
                        ->data($api_logs)
                        ->rule(array(
                            array(
                                'field' => 'api_id',
                                'label' => 'api ID',
                                'rules' => 'trim|required',
                            ),
                        ))
                        ->table_matching(array(
                            'api_id' => 'api_id',
                            'command_id' => 'command_id',
                            'request_id' => 'request_id',
                            'request_dt' => 'request_dt',
                            'client_id' => 'client_id',
                            'client_name' => 'client_name',
                            'request_url' => 'request_url',
                            'signature' => 'signature',
                            'filesend' => 'filesend',
                            'method' => 'method',
                        ))
                        ->exe();
                }
            }
        }

        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function index_delete($id = '', $file_name = '', $type = '')
    {
        // $file_name = base64_decode($file_name);
        if($type == 1){
          $file = '\\uploads\\temporary\\'.$file_name.".csv";
        }else{
          $file = '\\uploads\\success\\'.$file_name.".csv";
        }

        if(file_exists(getcwd() . $file)){
            $post = array();
            $post['status'] = 8;

            $data = $this->zainapi->update('t_file_commission')
                ->data($post)
                ->rule(array(
                    array(
                        'field' => 'status',
                        'label' => 'Status',
                        'rules' => 'trim|required',
                    ),
                ))
                ->table_matching(array(
                    'fc_process' => 'status',
                ))
                ->where(array('fc_id' => $id))
                ->exe();

            if ($data['error'] == 1) {
                $this->error = 1;
                $this->data = array(
                  "isdelete" => false,
                  "message"  => $data['message']
                );
            } else {
                unlink(getcwd() . $file);
                $this->error = 0;
                $this->data = array(
                  "isdelete" => true
                );
            }
        }else{
            $this->error = 0;
            $this->data = array(
              "isdelete" => false,
              "message"  => "File Not Found"
            );
        }
        return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);

    }
}
