<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Insurancefileresult extends REST_Backend {

    public function __construct() {
        parent::__construct();
    }

    public function index_get($id = '', $type = '') {

        if($type == 0){
            $table = 't_file_commission_extraction';
        }else{
            $table = 't_file_commission_exception';
        }

        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read($table)
            ->column();
        if(!empty($search)) {
            $query = $query->where('infiStatus != 0 AND (infiSerialNumber LIKE "%' . $search . '%" OR infiFileName LIKE "%' . $search . '%")');
        } else {
            $query = $query->where('fc_id = ' . $id);
        }
        if(!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if(!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if(empty($sort)) {
            $sort = 'asc';
        }
        if(!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        }else{
            $query = $query->order('fc_id asc');
        }
        $query = $query->render_pagination()->exe();
        $this->data = array(
          "data" => $query['data'],
          "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }
}
