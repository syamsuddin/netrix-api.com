<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Profile extends REST_Backend {

    public function __construct() {
        parent::__construct();
    }

    public function index_get() {
        $query = $this->zainapi->read('user')
            ->column(array(
                'userId' => 'id',
                'userUsername' => 'username',
                'userFullName' => 'fullname',
                'userPhone' => 'phone',
                'userPicture' => 'picture',
                'userRoleId' => 'role_id',
                'userCallerId' => 'caller_id',
                'roleName' => 'role_name',
            ))
			->join('role', 'roleId = userRoleId', 'LEFT')
            ->where(array('userId' => $this->auth_id))
            ->exe();

		$this->data = array(
              "data" => $query['data']
            );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function update_post() {
        $post = $this->input->post();

        $table_matching = array(
            'userPhone' => 'phone',
            'userRoleId' => 'role_id',
            'userCallerId' => 'caller_id',
        );

        if(!empty($_FILES)){
            $filename= $_FILES["picture"]["name"];
            $file_ext = pathinfo($filename,PATHINFO_EXTENSION);

            $id = $post["user_id"];
            $directory = '';

            $id_array = str_split($id);
            $directory = './uploads/profile';
            foreach($id_array as $v){
                $directory.="/{$v}";
                if(!is_dir($directory)) mkdir($directory, 0777);
            }
            $path = implode("/",$id_array);
            $post['asset_image'] = "/uploads/profile/" . $path . "/" . $id . "." . $file_ext;

            $config['overwrite'] = TRUE;
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['upload_path'] = $directory;
            $config['file_name'] = $id;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('picture')) {
                $this->error = 1;
                $this->data = array(
                  "isupdate" => false,
                  "message" => array(
                    "file" => $this->message = $this->upload->display_errors()
                  )
                );
                return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
            } else {
                $table_matching["userPicture"] = "asset_image";
            }
        }

        if(!empty($post['password'])){
            if($post['password'] != $post['conf_password']){
                $this->error = 1;
                $this->data = array(
                  "isupdate" => FALSE,
                  "message"  => array(
                      "Password" => "Password Not Match!"
                  )
                );
                return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
            }else{
                $post['password'] = $this->zaincode->pass_hash($post['password']);
                $table_matching["userPassword"] = "password";
            }
        }

        $data = $this->zainapi->update('user')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'phone',
                    'label' => 'Phone',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'role_id',
                    'label' => 'Role',
                    'rules' => 'trim|required|numeric',
                ),
                array(
                    'field' => 'caller_id',
                    'label' => 'Caller',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching($table_matching)
            ->where(array('userId' => $post["user_id"]))
            ->exe();

        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isupdate" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isupdate" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function password_post() {
        $post = $this->input->post();
		if($post['password'] != $post['conf_password']){
			$this->error = 1;
			$this->data = array(
				"data" =>'Password Not Match',
				 "isupdate" => false
			);
		}else{
			$post['password'] = $this->zaincode->pass_hash($post['password']);
			$data = $this->zainapi->update('user')
				->data($post)
				->rule(array(
					array(
						'field' => 'password',
						'label' => 'password',
						'rules' => 'trim|required',
					),
					array(
						'field' => 'conf_password',
						'label' => 'confirmation password',
						'rules' => 'trim|required',
					)
				))
				->table_matching(array(
					'userPassword' => 'password'
				))
				->where(array('userId' => $this->auth_id, 'userStatus !=' => 0))
				->exe();

			if ($data['error'] == 1) {
				$this->error = 1;
				$this->data = array(
					"data" => $data['message'],
					 "isupdate" => false
				);
			} else {
				$this->error = 0;
				$this->data = array(
				  "isupdate" => true
				);
			}
		}
		$this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

}
