<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Query_bonus extends REST_Backend
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index_post()
    {
      $this->load->helper('download');
      $post = $this->input->post();

      $file = './uploads/success/'.$post["requestid"].".csv";

      if(!file_exists($file)){
          $this->error = 1;
          $this->data = array(
            "isreceived" => false
          );
      } else {
          chdir("uploads/success/");
          $dir = getcwd();
          $file = $dir . "/" . $post["requestid"].".csv";
          // print_r(getcwd());exit();
          $ch      = curl_init($this->requesturl);
          $options = array(
            CURLOPT_RETURNTRANSFER      => true,
            CURLOPT_HEADER              => false,
            CURLOPT_FOLLOWLOCATION      => false,
            CURLOPT_AUTOREFERER         => true,
            CURLOPT_CONNECTTIMEOUT      => 20,
            CURLOPT_TIMEOUT             => 20,
            CURLOPT_POST                => 1,
            CURLOPT_POSTFIELDS          => array(
              'requestid' => $this->requestid,
              'isreceived' => "true",
              'attachmentfile' => new CurlFile($file,'text/csv',$post["requestid"].".csv")
            ),
            CURLOPT_SSL_VERIFYHOST      => 0,
            CURLOPT_SSL_VERIFYPEER      => false,
            CURLOPT_VERBOSE             => 1,
            CURLOPT_HTTPHEADER          => array(
               'responseid' => "file" . DATE("YmdHis"),
               'responsedt' => DATE("YmdHis"),
            )

          );
          curl_setopt_array($ch, $options);
          $data       = curl_exec($ch);
          $curl_errno = curl_errno($ch);
          $curl_error = curl_error($ch);

          print_r($data);
          print_r($curl_errno);
          print_r($curl_error);
          curl_close($ch);
          exit();

          $this->error = 0;
          $this->data = array(
            "isreceived" => true,
            "attachmentfile" => $post["requestid"],
          );

          $api_logs = array();
          $api_logs["api_id"] = 3;
          $api_logs["command_id"] = $this->commandid;
          $api_logs["request_id"] = $this->requestid;
          $api_logs["request_dt"] = $this->requestdt;
          $api_logs["client_id"] = $this->caller_id;
          $api_logs["client_name"] = $this->caller_name;
          $api_logs["request_url"] = $this->requesturl;
          $api_logs["signature"] = $this->signature;
          $api_logs["method"] = $_SERVER['REQUEST_METHOD'];

          $data = $this->zainapi->create('api_logs')
              ->data($api_logs)
              ->rule(array(
                  array(
                      'field' => 'api_id',
                      'label' => 'api ID',
                      'rules' => 'trim|required',
                  ),
              ))
              ->table_matching(array(
                  'api_id' => 'api_id',
                  'command_id' => 'command_id',
                  'request_id' => 'request_id',
                  'request_dt' => 'request_dt',
                  'client_id' => 'client_id',
                  'client_name' => 'client_name',
                  'request_url' => 'request_url',
                  'signature' => 'signature',
                  'method' => 'method',
              ))
              ->exe();
      }

      $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }
}
