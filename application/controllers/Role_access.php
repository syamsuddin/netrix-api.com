<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Role_access extends REST_Backend
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index_get($role_id = '')
    {
        $query2 = $this->zainapi->read('controllers')
            ->column(array(
                'cntrName' => 'name',
                'cntrId' => 'id',
            ))
            ->where("cntrStatus = 1")
            ->exe();

        $arr = array();

        foreach ($query2['data'] as $v) {
            $id = $v['id'];
            $query = $this->zainapi->read('role_access')
                    ->column(array(
                        'raccRoleId' => 'role_id',
                        'raccCntrId' => 'controllers_id',
                        'raccPost' => 'post',
                        'raccGet' => 'get',
                        'raccUpdate' => 'update',
                        'raccDelete' => 'delete',
                    ))
                    ->where("raccRoleId = $role_id AND raccCntrId = $id")
                    ->exe();


            if (!empty($query['data'])) {
                $query['data'][0]['id'] = $id;
                $query['data'][0]['name'] = $v['name'];
                array_push($arr, $query['data'][0]);
            } else {
                $query['data'][0]['id'] = $id;
                $query['data'][0]['role_id'] = $role_id;
                $query['data'][0]['controllers_id'] = $id;
                $query['data'][0]['name'] = $v['name'];
                array_push($arr, $query['data'][0]);
            }
        }

        $this->data = $arr;
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function index_post()
    {
        $post = $this->input->post();

        // Start Transaction
        // $this->zainapi->query_no_result('BEGIN TRAN');
        $this->db->trans_begin();
        // Lock Table
        // $this->zainapi->query_no_result('LOCK TABLE role_access WRITE');

        $arr = array();
        $error = 0;

        foreach ($post['cntr_id'] as $v) {
            $arr[$v] = array();
            $arr[$v]['role_id'] = $post['role_id'];
            $arr[$v]['cntr_id'] = $v;

            if (isset($post['post'][$v])) {
                $arr[$v]['post'] = 1;
            } else {
                $arr[$v]['post'] = 0;
            }

            if (isset($post['get'][$v])) {
                $arr[$v]['get'] = 1;
            } else {
                $arr[$v]['get'] = 0;
            }

            if (isset($post['update'][$v])) {
                $arr[$v]['update'] = 1;
            } else {
                $arr[$v]['update'] = 0;
            }

            if (isset($post['delete'][$v])) {
                $arr[$v]['delete'] = 1;
            } else {
                $arr[$v]['delete'] = 0;
            }

            // Check Status Item
            $check = $this->zainapi->read('role_access')
                    ->where(array('raccRoleId' => $arr[$v]['role_id'], 'raccCntrId' => $v))
                    ->exe();
            $data_detail = $check['data'];

            if (!empty($data_detail)) {
                $data = $this->zainapi->update('role_access')->where(array('raccRoleId' => $arr[$v]['role_id'], 'raccCntrId' => $v));
            } else {
                $data = $this->zainapi->create('role_access');
            }

            $data = $data->data($arr[$v])
                ->rule(array(
                    array(
                        'field' => 'role_id',
                        'label' => 'role_id',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'cntr_id',
                        'label' => 'controllers_id',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'post',
                        'label' => 'post',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'get',
                        'label' => 'get',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'update',
                        'label' => 'update',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'delete',
                        'label' => 'delete',
                        'rules' => 'required',
                    ),
                ))
                ->table_matching(array(
                    'raccRoleId' => 'role_id',
                    'raccCntrId' => 'cntr_id',
                    'raccPOST' => 'post',
                    'raccGET' => 'get',
                    'raccUPDATE' => 'update',
                    'raccDELETE' => 'delete',
                ))
                ->exe();

            if ($data['error'] == 1) {
                $this->zainapi->query_no_result('ROLLBACK');
                $this->error = 1;
                $error = 1;
                $this->data = array(
                  "isinsert" => false,
                  "message"  => $data['message']
                );
            }
        }

        if ($error == 1) {
            $this->zainapi->query_no_result('ROLLBACK');
            $this->error = 1;
            $this->data = array(
            "isinsert" => false,
            "message"  => $data['message']
          );
        } else {
            $this->zainapi->query_no_result('COMMIT TRAN');
            $this->error = 0;
            $this->data = array(
            "isinsert" => true
          );
        }

        // Unlock Table
        // $this->zainapi->query_no_result('UNLOCK TABLES');
        // Result API
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function update_post()
    {
        $post = $this->input->post();
        $data = $this->zainapi->update('role_access')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'role_name',
                    'label' => 'role name',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'role_id',
                    'label' => 'role id',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching(array(
                'roleName' => 'role_name',
            ))
            ->where(array('roleId' => $post['role_id'], 'roleStatus !=' => 0))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isupdate" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isupdate" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function index_delete($id = '')
    {
        $post = array();
        $post['status'] = 0;
        $post['role_id'] = $id;
        $data = $this->zainapi->update('role_access')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'role_id',
                    'label' => 'role id',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching(array(
                //'roleId' => 'role_id',
                'roleStatus' => 'status'
            ))
            ->where(array('roleId' => $post['role_id'], 'roleStatus !=' => 0))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isdelete" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isdelete" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }
}
