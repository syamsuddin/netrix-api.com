<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Tax extends REST_Backend
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index_get($id = '')
    {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('t_file_tax')
            ->column(
			array(
                'ft_id' => 'id',
                'insurance_id' => 'insurance_id',
                'ft_file' => 'file_name',
                'ft_period' => 'period',
                'ft_channel' => 'channel',
                'ft_save_dt' => 'save_time',
                'ft_process' => 'status',
                'caller_name' => 'insurance_name',
                'process_name' => 'process_name',
            )
			)
        ->join('t_caller', 't_caller.caller_id = insurance_id', 'LEFT')
        ->join('t_process', 't_process.process_id = ft_process', 'LEFT');
        if(!empty($this->get('caller'))){
            if($this->get('caller') != "ALL"){
                $query = $query->where('t_file_tax.insurance_id = ' . $this->get('caller'));
            }
        }
        if(!empty($this->get('date'))){
            // list($start, $end) = explode(' - ', $this->get('date'));
            // $start = DATE("Y-m-d",strtotime($start));
            // $end = DATE("Y-m-d",strtotime($end));
            // $query = $query->where("ft_save_dt > '" . $start . " 00:00:00' AND ft_save_dt < '" . $end . " 23:59:59'");
            $period = $this->get('date') . "-01";
            $query = $query->where("ft_period = '" . $period . "'");
        }
        if (!empty($search)) {
            $query = $query->where('ft_process != 0 AND (ft_id LIKE "%' . $search . '%" OR ft_file LIKE "%' . $search . '%")');
        } else {
            if (!empty($id)) {
                $query = $query->where(array('ft_id' => $id, 'ft_process !=' => 0));
            } else {
                $query = $query->where(array('ft_process !=' => 0));
            }
        }
        if (!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if (!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if (empty($sort)) {
            $sort = 'asc';
        }
        if (!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('ft_id' . ' ' . strtoupper('desc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
          "data" => $query['data'],
          "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function detailall_get($id = '')
    {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $this->db->select("*");
        $this->db->from("t_file_tax_extraction");
        $this->db->where("ft_id",$id);
        $query1 = $this->db->get_compiled_select(); // It resets the query just like a get()

        $this->db->select("*");
        $this->db->from("t_file_tax_exception");
        $this->db->where("ft_id",$id);
        $query2 = $this->db->get_compiled_select();

        $query = $this->zainapi->read("( " . $query1." UNION ALL ".$query2 . " ) as tem")->column();

        if (!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if (!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if (empty($sort)) {
            $sort = 'asc';
        }
        if (!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('ft_id' . ' ' . strtoupper('desc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
          "data" => $query['data'],
          "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function insurance_get($id_insurance = '')
    {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('t_file_tax')
            ->column(array(
                'ft_id' => 'id',
                'insurance_id' => 'insurance_id',
                'ft_file' => 'file_name',
                'ft_save_dt' => 'save_time',
                'ft_process' => 'status',
                'caller_name' => 'insurance_name',
                'process_name' => 'process_name',
            ))
        ->join('t_caller', 't_caller.caller_id = insurance_id', 'LEFT')
        ->join('t_process', 't_process.process_id = ft_process', 'LEFT');

        $query = $query->where(array('ft_process' => 14, 'insurance_id' => $id_insurance));

        if (!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if (!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if (empty($sort)) {
            $sort = 'asc';
        }
        if (!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('ft_id' . ' ' . strtoupper('asc'));
		}
        $query = $query->exe();
        $this->data = array(
          "data" => $query['data'],
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function sum_get($id = '', $type = '')
    {
        if($type == 0){
            $table = 't_file_tax_extraction';
        }else{
            $table = 't_file_tax_exception';
        }

        // Execute
        $query = $this->zainapi->read($table)
            ->column(array(
                'COUNT(ft_id)' => 'total',
            ))
            ->where(array('ft_id' => $id));
        $query = $query->exe();
        $this->data = array(
          "data" => $query['data']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function sp_get($id = '')
    {
        $sp = $this->zainapi->query_sp("DECLARE @EXEC_RESULT nvarchar(max) EXEC callBonusExtraction @PLAN_ID ={$id}, @EXEC_RESULT=@EXEC_RESULT OUTPUT; PRINT @EXEC_RESULT");
        list($code, $message) = explode(':', $sp);
        if($code == '00'){
            $query = $this->db->where('plan_id', $id)->update('t_file_tax', array('task_status' => 2));
            $this->data = array(
                "issuccess" => true,
            );
        }else{
            $this->data = array(
                "issuccess" => false,
                "message"  => array(
                    "Process" => "Process Store Procedure Failed"
                )
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function index_post()
    {
        $post = $this->input->post();

        if (empty($post["tax_period"])) {
            $this->error = 1;
            $this->data = array(
              "isinsert" => false,
              "message"  => array(
              "Period" => "Please select period"
              )
            );

            return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
        }

        if (empty($_FILES["tax_file"])) {
            $this->error = 1;
            $this->data = array(
              "isinsert" => false,
              "message"  => array(
              "file" => "FILE UPLOAD EMPTY"
              )
            );
        } else {
			$_FILES["tax_file"]["name"] = str_replace(" ","_",$_FILES["tax_file"]["name"]);
            $arr_type = array("text/csv", "application/vnd.ms-excel");
            if (!in_array($_FILES["tax_file"]["type"], $arr_type)) {
                $this->data = array(
                  "isinsert" => false,
                  "message"  => array(
                    "file" => "Format must csv"
                  )
                );
                $this->message = array(
                  "code" => "01",
                  "message" => "File should csv format"
                );
            } else {
                $query = $this->zainapi->read('t_file_tax')
                    ->column(array(
                        'ft_id' => 'id',
                        'insurance_id' => 'insurance_id',
                        'ft_file' => 'file_name',
                        'ft_save_dt' => 'save_time',
                        'ft_process' => 'status',
                    ))
                    ->where(array('ft_file LIKE' => '%' . $_FILES["tax_file"]["name"] . '%'))
                    ->exe();

                if(!empty($query['data'])){
                    $do_upload = FALSE;
                    $status = 2;
                }else{
                    $do_upload = TRUE;
                    $status = 1;
                }

                $directory = './uploads/tax/';
                $config['upload_path']          = $directory;
                $config['allowed_types']        = 'csv';
                $config['max_size']             = 15000;
                $config['file_name']            = $this->requestid . "_" . $_FILES["tax_file"]["name"];

                $this->load->library('upload', $config);

                if($do_upload){
                    if (!$this->upload->do_upload('tax_file')) {
                        $this->error = 1;
                        $this->data = array(
                          "isreceived" => false,
                          "message" => $this->message = $this->upload->display_errors()
                        );

                        return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
                    }
                }

                date_default_timezone_set("Asia/Jakarta");
                $datas = array(
                    "insurance_id" => $post["tax_insurance"],
                    "file" => $config['file_name'],
                    "period" => $post["tax_period"] . "-01",
                    "channel" => "WEB",
                    "save" => date("Y-m-d H:i:s"),
                    "process" => $status,
                );

                $data = $this->zainapi->create('t_file_tax')
                    ->data($datas)
                    ->rule(array(
                        array(
                            'field' => 'insurance_id',
                            'label' => 'insurance',
                            'rules' => 'trim|required',
                        ),
                    ))
                    ->table_matching(array(
                        'insurance_id' => 'insurance_id',
                        'ft_file' => 'file',
                        'ft_period' => 'period',
                        'ft_channel' => 'channel',
                        'ft_save_dt' => 'save',
                        'ft_process' => 'process',
                    ))
                    ->exe();
                if ($data['error'] == 1) {
                    $this->error = 1;
                    $this->data = array(
                      "isrecevied" => false,
                      "message"  => $data['message']
                    );
                    unlink($directory.$config['file_name']);
                } else {
                    $this->error = 0;
                    $this->data = array(
                      "requestid" => $_SERVER['HTTP_REQUESTID'],
                      "isrecevied" => true
                    );

                    $api_logs = array();
                    $api_logs["api_id"] = 4;
                    $api_logs["command_id"] = $this->commandid;
                    $api_logs["request_id"] = $this->requestid;
                    $api_logs["request_dt"] = $this->requestdt;
                    $api_logs["client_id"] = $this->caller_id;
                    $api_logs["client_name"] = $this->caller_name;
                    $api_logs["request_url"] = $this->requesturl;
                    $api_logs["signature"] = $this->signature;
                    $api_logs["filesend"] = $config['file_name'];
                    $api_logs["method"] = $_SERVER['REQUEST_METHOD'];

                    $data = $this->zainapi->create('api_logs')
                        ->data($api_logs)
                        ->rule(array(
                            array(
                                'field' => 'api_id',
                                'label' => 'api ID',
                                'rules' => 'trim|required',
                            ),
                        ))
                        ->table_matching(array(
                            'api_id' => 'api_id',
                            'command_id' => 'command_id',
                            'request_id' => 'request_id',
                            'request_dt' => 'request_dt',
                            'client_id' => 'client_id',
                            'client_name' => 'client_name',
                            'request_url' => 'request_url',
                            'signature' => 'signature',
                            'filesend' => 'filesend',
                            'method' => 'method',
                        ))
                        ->exe();
                }
            }
        }

        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function index_delete($id = '', $file_name = '', $type = '')
    {
        $file_name = base64_decode($file_name);
        $post = array();
        $data = $this->zainapi->delete('t_file_tax_extraction')
            ->where(array('ft_id' => $id))
            ->exe();
        $data = $this->zainapi->delete('t_file_tax')
            ->where(array('ft_id' => $id))
            ->exe();

        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isdelete" => false,
              "message"  => $data['message']
            );
        } else {
            $file = './uploads/tax_success/'.$file_name;
            unlink($file);
            $this->error = 0;
            $this->data = array(
              "isdelete" => true
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function detail_get($id = '', $type = '') {

        if($type == 0){
            $table = 't_file_tax_extraction';
        }else{
            $table = 't_file_tax_exception';
        }

        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read($table)
            ->column();
		$query = $query->where('ft_id = ' . $id);
        if(!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if(!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if(empty($sort)) {
            $sort = 'asc';
        }
        if(!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        }else{
            $query = $query->order('ft_id asc');
        }
        $query = $query->render_pagination()->exe();
        $this->data = array(
          "data" => $query['data'],
          "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }
}
