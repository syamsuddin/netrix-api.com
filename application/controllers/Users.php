<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Users extends REST_Backend {

    public function __construct() {
        parent::__construct();
    }

	public function index_get($id = '') {
        // Paging Param
        $item_per_page = $this->get('item_per_page');
        $page = $this->get('page');
        $search = $this->get('search');
        $order = $this->get('order');
        $sort = $this->get('sort');
        // Execute
        $query = $this->zainapi->read('user')
            ->column(array(
                'userId' => 'id',
                'userUsername' => 'username',
                'userFullName' => 'fullname',
                'userPhone' => 'phone',
                'userRoleId' => 'role_id',
                'userCallerId' => 'caller_id',
                'roleName' => 'role_name',
                'userStatus' => 'status',
                'userPicture' => 'picture',
                'caller_name' => 'caller_name',
            ))
        ->join('role', 'roleId = userRoleId', 'LEFT')
        ->join('t_caller', 'caller_id = userCallerId', 'LEFT');
        if(!empty($this->get('fullname'))){
            $query = $query->where("userFullName LIKE '%" . $this->get('fullname') . "%'");
        }
        if(!empty($this->get('username'))){
            $query = $query->where("userUsername LIKE '%" . $this->get('username') . "%'");
        }
        if(!empty($this->get('phone'))){
            $query = $query->where("userPhone LIKE '%" . $this->get('phone') . "%'");
        }
        if(!empty($this->get('role_id'))){
            if($this->get('role_id') != "ALL"){
                $query = $query->where('userRoleId = ' . $this->get('role_id'));
            }
        }
        if(!empty($this->get('caller_id'))){
            if($this->get('caller_id') != "ALL"){
                $query = $query->where('userCallerId = ' . $this->get('caller_id'));
            }
        }
        if(!empty($this->get('status'))){
            if($this->get('status') != "ALL"){
                $query = $query->where('userStatus = ' . $this->get('status'));
            }
        }
        if(!empty($search)) {
            $query = $query->where('userUsername LIKE "%' . $search . '%" OR userFullName LIKE "%' . $search . '%"');
        } else {
            if(!empty($id)) {
                $query = $query->where('userId = ' . $id);
            } else {
                $query = $query->where();
            }
        }
        if(!empty($item_per_page)) {
            $query = $query->item_per_page($item_per_page);
        }
        if(!empty($page)) {
            $query = $query->page($page);
        } else {
            $query = $query->page(1);
        }
        if(empty($sort)) {
            $sort = 'asc';
        }
        if(!empty($order)) {
            $query = $query->order($order . ' ' . strtoupper($sort));
        } else {
			$query = $query->order('userId' . ' ' . strtoupper('asc'));
		}
        $query = $query->render_pagination()->exe();
        $this->data = array(
          "data" => $query['data'],
          "pagination" => $query['pagination']
        );
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function index_post() {
        $post = $this->input->post();
        if (empty($_FILES["user_file"])) {
            $this->error = 1;
            $this->data = array(
              "isinsert" => false,
              "message"  => array(
                "file" => "FILE UPLOAD EMPTY"
              )
            );
          } else {

            if(!empty($post['password'])) {
                $post['password'] = $this->zaincode->pass_hash($post['password']);
            }

            // Start Transaction
            // $this->zainapi->query_no_result('BEGIN TRANSACTION');

            $data = $this->zainapi->create('user')
                ->data($post)
                ->unique('user', 'username', 'userUsername', 'userStatus != 0')
                ->rule(array(
                    array(
                        'field' => 'username',
                        'label' => 'username',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'password',
                        'label' => 'password',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'fullname',
                        'label' => 'fullname',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'phone',
                        'label' => 'phone',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'role_id',
                        'label' => 'role id',
                        'rules' => 'trim|required|numeric',
                    ),
                ))
                ->table_matching(array(
                    'userUsername' => 'username',
                    'userPassword' => 'password',
                    'userFullName' => 'fullname',
                    'userPhone' => 'phone',
                    'userRoleId' => 'role_id',
                    'userCallerId' => 'caller_id',
                ))
                ->exe();
            if ($data['error'] == 1) {
                $this->error = 1;
                $this->data = array(
                  "isinsert" => FALSE,
                  "message"  => $data['message']
                );
            } else {
                $query_last_id = $this->zainapi->read('user')
                  ->column(array(
                      'userId' => 'id'
                  ));
                $query_last_id = $query_last_id->limit(1);
                $query_last_id = $query_last_id->order('userId Desc')->exe();

                $filename= $_FILES["user_file"]["name"];
                $file_ext = pathinfo($filename,PATHINFO_EXTENSION);

                $id = $query_last_id["data"][0]["id"];
                $directory = '';
                if(!empty($_FILES)) {
                    $id_array = str_split($id);
                    $directory = './uploads/profile';
                    foreach($id_array as $v){
                        $directory.="/{$v}";
                        if(!is_dir($directory)) mkdir($directory, 0777);
                    }
                    $path = implode("/",$id_array);
                    $post['asset_image'] = "/uploads/profile/" . $path . "/" . $id . "." . $file_ext;
                }

                $config['overwrite'] = TRUE;
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['upload_path'] = $directory;
                $config['file_name'] = $id;

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('user_file')) {
                    $this->error = 1;
                    $this->data = array(
                      "isinsert" => false,
                      "message" => array(
                        "file" => $this->message = $this->upload->display_errors()
                      )
                    );
                    $this->zainapi->query_no_result('ROLLBACK');
                } else {
                    // Update Data Gambar
                    $data_update = $this->zainapi->update('user')
                        ->data($post)
                        ->rule()
                        ->table_matching(array(
                            'userPicture' => 'asset_image'
                        ))
                        ->where(array('userId' => $id, 'userStatus !=' => 0))
                        ->exe();

                    // Return Message
                    if ($data_update['error'] == 1) {
                        $this->error = 1;
                        $this->data = array(
                          "isinsert" => false,
                          "message"  => $data['message']
                        );
                        // $this->zainapi->query_no_result('ROLLBACK');
                    } else {
                        $this->error = 0;
                        $this->data = array(
                          "isinsert" => true
                        );
                        // $this->zainapi->query_no_result('COMMIT');
                    }
                }
            }
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }


	public function update_post() {
        $post = $this->input->post();

        $table_matching = array(
            'userPhone' => 'phone',
            'userRoleId' => 'role_id',
            'userCallerId' => 'caller_id',
        );

        if(!empty($_FILES)){
            $filename= $_FILES["picture"]["name"];
            $file_ext = pathinfo($filename,PATHINFO_EXTENSION);

            $id = $post["user_id"];
            $directory = '';

            $id_array = str_split($id);
            $directory = './uploads/profile';
            foreach($id_array as $v){
                $directory.="/{$v}";
                if(!is_dir($directory)) mkdir($directory, 0777);
            }
            $path = implode("/",$id_array);
            $post['asset_image'] = "/uploads/profile/" . $path . "/" . $id . "." . $file_ext;

            $config['overwrite'] = TRUE;
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['upload_path'] = $directory;
            $config['file_name'] = $id;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('picture')) {
                $this->error = 1;
                $this->data = array(
                  "isupdate" => false,
                  "message" => array(
                    "file" => $this->message = $this->upload->display_errors()
                  )
                );
                return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
            } else {
                $table_matching["userPicture"] = "asset_image";
            }
        }

        if(!empty($post['password'])){
            if($post['password'] != $post['conf_password']){
                $this->error = 1;
                $this->data = array(
                  "isupdate" => FALSE,
                  "message"  => array(
                      "Password" => "Password Not Match!"
                  )
                );
                return $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
            }else{
                $post['password'] = $this->zaincode->pass_hash($post['password']);
                $table_matching["userPassword"] = "password";
            }
        }

        $data = $this->zainapi->update('user')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'phone',
                    'label' => 'Phone',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'role_id',
                    'label' => 'Role',
                    'rules' => 'trim|required|numeric',
                ),
                array(
                    'field' => 'caller_id',
                    'label' => 'Caller',
                    'rules' => 'trim|required|numeric',
                ),
            ))
            ->table_matching($table_matching)
            ->where(array('userId' => $post['user_id']))
            ->exe();

        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isupdate" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isupdate" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

    public function status_post($id = "", $status = "") {
        $post["id"] = $id;
        $post["status"] = $status;
        $data = $this->zainapi->update('user')
            ->data($post)
            ->rule(array(
                array(
                    'field' => 'id',
                    'label' => 'id',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'status',
                    'label' => 'status',
                    'rules' => 'trim|required',
                ),
            ))
            ->table_matching(array(
                'userStatus' => 'status',
            ))
            ->where(array('userId' => $post['id']))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isupdate" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isupdate" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

	public function index_delete($id = '') {
		$post = array();
        $post['status'] = 0;
        $post['user_id'] = $id;
        $data = $this->zainapi->delete('user')
            ->where(array('userId' => $post['user_id']))
            ->exe();
        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isdelete" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isdelete" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
	}

    public function deleteall_post() {
		$post = $this->input->post();
        $arr_id =  implode(",",$post["data"]);

        $data = $this->db->where_in('userId', $arr_id)
            ->delete('user');

        if ($data['error'] == 1) {
            $this->error = 1;
            $this->data = array(
              "isdelete" => FALSE,
              "message"  => $data['message']
            );
        } else {
            $this->error = 0;
            $this->data = array(
              "isdelete" => TRUE
            );
        }
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
	}

	public function profile_get() {
         $query = $this->zainapi->read('user')
            ->column(array(
                'userUsername' => 'username',
                'userFullName' => 'fullname',
                'userPhone' => 'phone',
                'roleName' => 'role_name'
            ))
			->join('role', 'roleId = userRoleId', 'LEFT')
            ->where(array('userStatus !=' => 0, 'userId' => $this->auth_id))
            ->exe();
        $this->data = $query['data'];
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }

}
