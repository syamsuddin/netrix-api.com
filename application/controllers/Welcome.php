<?php
use Restserver\Libraries\REST_Backend;
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Backend.php';

class Welcome extends REST_Backend {

    public function __construct() {
        parent::__construct();
    }

    public function index_get() {
        $this->message = 'No Input Specified';
        $this->set_response($this->generate_result(), REST_Controller::HTTP_OK);
    }
}
