<?php
namespace Restserver\Libraries;

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class REST_Backend extends REST_Controller
{

    protected $error = 0;
    protected $message = 'no error';
    protected $data = array();
    protected $class_name;
    protected $method_name;
    protected $request_method;
    protected $auth_id;
    protected $auth_role;
    protected $uuid;
    protected $token;
    protected $render_pagination;

    public function __construct()
    {
        parent::__construct();

        $this->commandid = $this->input->get_request_header('commandid');
        $this->requestid = $this->input->get_request_header('requestid');
        $this->requestdt = $this->input->get_request_header('requestdt');
        $this->clientid = $this->input->get_request_header('clientid');
        $this->signature = $this->input->get_request_header('signature');
        $this->requesturl = $this->input->get_request_header('requesturl');
        $this->uuid = $this->input->get_request_header('uuid');
        $this->token = $this->input->get_request_header('token');

        if ($this->router->fetch_class() == 'auth' and  $this->router->fetch_method() == 'index' and $_SERVER['REQUEST_METHOD'] == 'POST'){

        }else{
          if (empty($this->input->server('PHP_AUTH_USER')) || empty($this->input->server('PHP_AUTH_PW'))){
             header('HTTP/1.0 401 Unauthorized');
             header('HTTP/1.1 401 Unauthorized');
             header('WWW-Authenticate: Basic realm="My Realm"');
             echo 'You must login to use this service';
             die();
          }

          $username_ba = $this->input->server('PHP_AUTH_USER');
          $password_ba = $this->input->server('PHP_AUTH_PW');
          // $password_ba = $this->zaincode->pass_hash($password_ba);

          $username = "";
          $secret_key = "";

          $query_ba = $this->zainapi->read('t_caller')
            ->column(array(
              'caller_id'=>'id',
              'caller_auth_header'=>'username',
              'caller_key_secret'=>'secret_key',
              'caller_name'=>'caller_name',
              'caller_outbound'=>'caller_outbound',
            ))
            ->where(array('caller_auth_header' => $username_ba, 'caller_auth_password' => $password_ba, 'caller_status' => "ACTIVE"))
            ->exe();

          if(empty($query_ba['data'])){
            header('HTTP/1.0 401 Unauthorized');
            header('HTTP/1.1 401 Unauthorized');
            header('WWW-Authenticate: Basic realm="My Realm"');
            echo 'You must login to use this service'; // User sees this if hit cancel
            die();
          }else{
            $username = $query_ba['data'][0]['username'];
            $secret_key = $query_ba['data'][0]['secret_key'];
            $this->caller_id = $query_ba['data'][0]['id'];
            $this->caller_name = $query_ba['data'][0]['caller_name'];
            $this->caller_outbound = $query_ba['data'][0]['caller_outbound'];
          }

          $api_list_query = $this->zainapi->read('t_api')
            ->column(array(
              'api_id'=>'api_id',
              'api_name'=>'api_name',
              'api_method_get'=>'api_method_get',
              'api_method_post'=>'api_method_post',
            ))
            ->exe();

          $api_list = array();

          foreach($api_list_query["data"] as $val){
              array_push($api_list,$val["api_name"]);
          }

          if(in_array($this->commandid, $api_list)){

              $method = $_SERVER['REQUEST_METHOD'];
              $method = "api_method_". strtolower($method);

              $check_access = $this->zainapi->read('t_caller_api_access')
                ->column(array(
                  't_caller_api_access.caller_id'=>'caller_id',
                  't_caller_api_access.api_id'=>'api_id',
                  't_api.api_name'=>'api_name',
                ))
                ->join('t_api','t_api.api_id = t_caller_api_access.api_id', 'LEFT')
                ->where(array(
                    't_caller_api_access.caller_id' => $query_ba['data'][0]['id'],
                    't_api.api_name' => $this->commandid,
                    $method => "Y",
                ))
                ->exe();

              if(empty($check_access["data"])){

                  $this->message = array(
                    "code" => "101",
                    "message" => "Permission Denied"
                  );

                  $return = array(
                      'header' => array(
                        "responseid" => $this->requestid,
                        "responsedt" => DATE("Y-m-d H:i:s"),
                      ),
                      'body' => $this->data,
                      'status' => $this->message
                  );

                  return $this->response($this->generate_result(), REST_Controller::HTTP_UNAUTHORIZED);
              }
          }

          $this->message = array(
            "code" => "00",
            "message" => "Request is Succesfully"
          );

          $string_to_sign = $secret_key . $username . $this->requestdt;
          $signature = hash_hmac('sha256', $string_to_sign, $secret_key);

          $timeFirst  = strtotime(date("Y-m-d H:i:s"));
          $timeSecond = strtotime($this->requestdt);
          $differenceInSeconds = $timeSecond - $timeFirst;

          if($signature != $this->signature && $differenceInSeconds < 60){
            $this->response($this->generate_result(), REST_Controller::HTTP_UNAUTHORIZED);
          }

      		$query_session = $this->zainapi->read('token')
      			->column(array(
      				'tkenAuthId'=>'auth_id'
      			))
      			->where(array('tkenHASH' => $this->token))
      			->where(array('tkenUUID' => $this->uuid))
            ->exe();
      		if(!empty($query_session['data'])){
      			$this->auth_id =  $query_session['data'][0]['auth_id'];
      		}

          $this->class_name = $this->router->fetch_class();

          if($this->class_name != "auth"){
            $post = array();
            $post["api_name"] = $this->commandid;
            $post["api_method"] = $_SERVER['REQUEST_METHOD'];
            $post["api_status"] = "ACTIVE";
            $post["system_creation_dt"] = DATE("Y-m-d H:i:s");
            $post["system_creation_name"] = $username;

          }
        }
    }

    protected function generate_result()
    {
        // Log Query
        $this->zainapi->log_query($this->auth_id);

        // Result API
        $return = array(
            'header' => array(
              "responseid" => $this->requestid,
              "responsedt" => DATE("Y-m-d H:i:s"),
            ),
            'body' => $this->data,
            'status' => $this->message
        );

        return $return;
    }

}
