<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Zainapi
{

    private $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    public function create($table = '')
    {
        require_once APPPATH . 'libraries/Zainapi/Create.php';
        return new Zainapi_create($table);
    }

    public function read($table = '')
    {
        require_once APPPATH . 'libraries/Zainapi/Read.php';
        return new Zainapi_read($table);
    }

    public function update($table = '')
    {
        require_once APPPATH . 'libraries/Zainapi/Update.php';
        return new Zainapi_update($table);
    }

    public function delete($table = '')
    {
        require_once APPPATH . 'libraries/Zainapi/Delete.php';
        return new Zainapi_delete($table);
    }

    public function query($query = '') {
        $return = array();
        if(!empty($query)) {
            $return = $this->CI->db->query($query)->result_array();
        }
        return $return;
    }

	 public function query_sp($query = '') {
        $return = '';
        if(!empty($query)) {
			if ( ! $this->CI->db->simple_query($query)) {
					$res = $this->CI->db->error();
			}
			if(!empty($res['message'])) {
				$ex = explode(']', $res['message']);
				$return = @$ex[COUNT($ex) - 1];
			}
        }
        return $return;
    }

    public function query_no_result($query = '') {
        if(!empty($query)) {
            $return = $this->CI->db->query($query);
        }
    }

    public function auth($table = '', $parameter = array(), $key_return = '')
    {
        $return = false;
        if (!empty($parameter)) {
            foreach ($parameter as $v_parameters) {
                $this->CI->db->where($v_parameters['column'], $v_parameters['value']);
            }
        }
        $data = $this->CI->db->get($table)->result_array();
        $count = COUNT($data);
        if (!empty($count)) {
            if (!empty($key_return)) {
                if (!empty($data[0][$key_return])) {
                    $return = $data[0][$key_return];
                } else {
                    $return = true;
                }
            } else {
                $return = true;
            }
        }
        return $return;
    }

    public function check_duplicate($table = '', $field = '', $parameters = array())
    {
        $return = '';
        if (!empty($parameters)) {
            foreach ($parameters as $v_parameters) {
                if (!empty($v_parameters['required']) and $v_parameters['required'] == true) {
                    if ($v_parameters['value'] == '') {
                        $return = 'Bidang ' . $field . ' dibutuhkan';
                        return $return;exit();
                    }
                }
                $this->CI->db->where($v_parameters['column'], $v_parameters['value']);
            }
            $data = $this->CI->db->get($table)->result_array();
            $count = COUNT($data);
            if (!empty($count)) {
                $return = 'Bidang ' . $field . ' sudah ada di database sistem';
            }
        }
        return $return;
    }

    public function log_query($auth_id = '') {
        // $filepath = APPPATH . 'logs/ql-' . date('Y-m-d') . '.txt'; // Filepath. File is created in logs folder with name QueryLog
        // $handle = fopen($filepath, "a+"); // Open the file
        //
        // $times = $this->CI->db->query_times; // We get the array of execution time of each query that got executed by our application(controller)
        //
        // foreach ($this->CI->db->queries as $key => $query) { // Loop over all the queries  that are stored in db->queries array
        //     $sql = "Auth ID : " . $auth_id . " | Date Time : " . date('Y-m-d H:i:s') . "\n" . $query . "\nExecution Time : " . $times[$key]; // Write it along with the execution time
        //     fwrite($handle, $sql . "\n\n");
        // }

        //fwrite($handle, "\n\n========\n\n");

        //fclose($handle); // Close the file
    }

}
