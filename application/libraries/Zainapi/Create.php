<?php

class Zainapi_create {

    private $CI;
    private $table;
    private $data = array();
    private $rule = array();
    private $table_matching = array();
    private $unique_table = array();
    private $unique_field = array();
    private $unique_param = array();
    private $unique_col = array();
    private $return = array();

    public function __construct($table = '') {
        $this->CI = & get_instance();
        $this->table = $table;
        $this->return = array(
            'error' => 0,
            'message' => array(),
            'data' => array()
        );
    }

    public function data($data = array()) {
        $this->data = $data;
        return $this;
    }

    public function rule($rule = array()) {
        $this->rule = $rule;
        return $this;
    }

    public function table_matching($table_matching = array()) {
        $this->table_matching = $table_matching;
        return $this;
    }

    public function unique($table = '', $field = '', $col = '', $param = '') {
        $this->unique_table[] = $table;
        $this->unique_field[] = $field;
        $this->unique_param[] = $param;
        $this->unique_col[] = $col;
        return $this;
    }

    public function exe() {
        // Validation
        $this->CI->load->library('form_validation');
        $this->CI->form_validation->set_data($this->data);
        $this->CI->form_validation->set_rules($this->rule);
        if ($this->CI->form_validation->run() == FALSE) {
            $this->return['error'] = 1;
            $this->return['message'] = $this->CI->form_validation->error_array();
        } else {
            // Check Unique
            if(!empty($this->unique_table)) {
                foreach($this->unique_table as $k_ut => $v_ut) {
                    $data_unique = array();
                    $this->CI->db->select();
                    if(!empty($this->unique_param[$k_ut])) {
                        if(is_array($this->unique_param[$k_ut])) {
                            foreach($this->unique_param[$k_ut] as $v_params_unique) {
                                $this->CI->db->where($v_params_unique);
                            }
                        } else {
                            $this->CI->db->where($this->unique_param[$k_ut]);
                        }
                    }
                    if(!empty($this->unique_col[$k_ut]) AND !empty($this->unique_field[$k_ut])) {
                        $this->CI->db->where($this->unique_col[$k_ut], $this->data[$this->unique_field[$k_ut]]);
                    }
                    if(!empty($this->unique_table[$k_ut])) {
                        $data_unique = $this->CI->db->get($this->unique_table[$k_ut])->result_array();
                    }
                    if(!empty($data_unique)) {
                        $this->return['error'] = 1;
                        $this->return['message'][$this->unique_field[$k_ut]] = 'The value ' . $this->unique_field[$k_ut] . ' has been already saved in database';
                    }
                }
            }
            if(empty($this->return['error'])) {
                // Save To Database
                $data_insert = array();
                if (!empty($this->table_matching)) {
                    foreach ($this->table_matching as $k_tm => $v_tm) {
                        $data_insert[$k_tm] = @$this->data[$v_tm];
                    }
                }
                $save = $this->CI->db->insert($this->table, $data_insert);
                if ($save === true) {
                    $this->return['message'] = 'saved';
                    $this->return['data'] = array(
                        // 'last_insert_id' => $this->CI->db->insert_id()
                    );
                } else {
                    $this->return['error'] = 1;
                    $this->return['message'] = 'unsaved';
                }
            }
        }
        return $this->return;
    }

}
