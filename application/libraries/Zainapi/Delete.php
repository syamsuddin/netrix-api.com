<?php

class Zainapi_delete {

    private $CI;
    private $table;
    private $where_column = array();
    private $where_param = array();
    private $return = array();

    public function __construct($table = '') {
        $this->CI = & get_instance();
        $this->table = $table;
        $this->return = array(
            'error' => 0,
            'message' => '',
            'data' => array()
        );
    }

    public function where($where_param = array()) {
        if (!empty($where_param) AND is_array($where_param)) {
            foreach ($where_param as $k_wp => $v_wp) {
                $this->where_column[] = $k_wp;
                $this->where_param[] = $v_wp;
            }
        }
        return $this;
    }

    public function exe() {
        // Where Parameter
        if (!empty($this->where_column) AND ! empty($this->where_param)) {
            foreach ($this->where_column as $k_wc => $v_wc) {
                $this->CI->db->where($this->where_column[$k_wc], $this->where_param[$k_wc]);
            }
        }
        // Delete
        $delete = $this->CI->db->delete($this->table);
        if ($delete === true) {
            $this->return['message'] = 'deleted';
            $this->return['data'] = array(
                'affected_row' => $this->CI->db->affected_rows()
            );
        } else {
            $this->return['error'] = 1;
            $this->return['message'] = 'unsaved';
        }
        return $this->return;
    }

}
