<?php

class Zainapi_read
{

    private $CI;
    private $table;
    private $column;
    private $join_table = array();
    private $join_param = array();
    private $join_type = array();
    private $where_column = array();
    private $where_param = array();
    private $where_string = array();
    private $where_in_column = '';
    private $where_in_param = array();
    private $order = '';
    private $to_many_table = '';
    private $to_many_param = array();
    private $to_many_key = '';
    private $to_many_join_table = '';
    private $to_many_join_param = '';
    private $to_many_join_type = '';
    private $limit_param = '';
    private $render_pagination = false;
    private $item_per_page = 10;
    private $page = 1;
    private $return = array();

    public function __construct($table = '')
    {
        $this->CI = &get_instance();
        $this->table = $table;
        $this->return = array(
            'error' => 0,
            'message' => '',
            'pagination' => '',
            'data' => array(),
        );
    }

    public function column($column = array())
    {
        $this->column = $column;
        return $this;
    }

    public function join($join_table = '', $join_param = '', $join_type = 'left')
    {
        $this->join_table[] = $join_table;
        $this->join_param[] = $join_param;
        $this->join_type[] = $join_type;
        return $this;
    }

    public function where($where_param = array())
    {
        if (!empty($where_param) and is_array($where_param)) {
            foreach ($where_param as $k_wp => $v_wp) {
                $this->where_column[] = $k_wp;
                $this->where_param[] = $v_wp;
            }
        } else if (!empty($where_param) and !is_array($where_param)) {
            $this->where_string[] = $where_param;
        }
        return $this;
    }

    public function where_in($column = '', $param = array()) {
        if(!empty($column)) {
            $this->where_in_column = $column;
        }
        if(!empty($param)) {
            $this->where_in_param = $param;
        }
        return $this;
    }

    public function to_many($table = '', $param = array(), $key, $join_table = '', $join_param = '', $join_type = '')
    {
        $this->to_many_table = $table;
        $this->to_many_param = $param;
        $this->to_many_key = $key;
        $this->to_many_join_table = $join_table;
        $this->to_many_join_param = $join_param;
        $this->to_many_join_type = $join_type;
        return $this;
    }

    public function order($param = '')
    {
        $this->order = $param;
        return $this;
    }

    public function limit($param = '')
    {
        $this->limit_param = $param;
        return $this;
    }

    public function item_per_page($item_per_page = 10) {
        $this->item_per_page = $item_per_page;
        return $this;
    }

    public function page($page = 1) {
        $this->page = $page;
        return $this;
    }

    public function render_pagination() {
        $this->render_pagination = true;
        return $this;
    }

    public function exe()
    {
        // Render Pagination
        if($this->render_pagination) {
            // **************************************************
            // Counting All Data
            $q_select = '';
            if (!empty($this->column)) {
                $count_col = COUNT($this->column);
                $cc = 1;
                foreach ($this->column as $k_col => $v_col) {
                    $comma = ', ';
                    if ($cc >= $count_col) {
                        $comma = '';
                    }
                    $q_select .= $k_col . ' AS ' . $v_col . $comma;
                    $cc++;
                }
            }
            $this->CI->db->select($q_select);
            $this->CI->db->from($this->table);
            // Join Table
            if (!empty($this->join_table) and !empty($this->join_param) and !empty($this->join_type)) {
                foreach ($this->join_table as $k_jt => $v_jt) {
                    $this->CI->db->join($this->join_table[$k_jt], $this->join_param[$k_jt], $this->join_type[$k_jt]);
                }
            }
            // Where Parameter
            if (!empty($this->where_column) and !empty($this->where_param)) {
                foreach ($this->where_column as $k_wc => $v_wc) {
                    $this->CI->db->where($this->where_column[$k_wc], $this->where_param[$k_wc]);
                }
            }
            // Where Parameter String
            if (!empty($this->where_string)) {
                foreach ($this->where_string as $v_ws) {
                    $this->CI->db->where($v_ws);
                }
            }
            // Where In
            if(!empty($this->where_in_column) AND !empty($this->where_in_param)) {
                $this->CI->db->where_in($this->where_in_column, $this->where_in_param);
            }
            // Ordering
            if (!empty($this->order)) {
                $this->CI->db->order_by($this->order);
            }
            $data_count = $this->CI->db->count_all_results();
            // **************************************************
            // Limit And Offset Parameter
            $q_select = '';
            if (!empty($this->column)) {
                $count_col = COUNT($this->column);
                $cc = 1;
                foreach ($this->column as $k_col => $v_col) {
                    $comma = ', ';
                    if ($cc >= $count_col) {
                        $comma = '';
                    }
                    $q_select .= $k_col . ' AS ' . $v_col . $comma;
                    $cc++;
                }
            }
            $this->CI->db->select($q_select);
            $this->CI->db->from($this->table);
            // Join Table
            if (!empty($this->join_table) and !empty($this->join_param) and !empty($this->join_type)) {
                foreach ($this->join_table as $k_jt => $v_jt) {
                    $this->CI->db->join($this->join_table[$k_jt], $this->join_param[$k_jt], $this->join_type[$k_jt]);
                }
            }
            // Where Parameter
            if (!empty($this->where_column) and !empty($this->where_param)) {
                foreach ($this->where_column as $k_wc => $v_wc) {
                    $this->CI->db->where($this->where_column[$k_wc], $this->where_param[$k_wc]);
                }
            }
            // Where Parameter String
            if (!empty($this->where_string)) {
                foreach ($this->where_string as $v_ws) {
                    $this->CI->db->where($v_ws);
                }
            }
            // Where In
            if(!empty($this->where_in_column) AND !empty($this->where_in_param)) {
                $this->CI->db->where_in($this->where_in_column, $this->where_in_param);
            }
            // Ordering
            if (!empty($this->order)) {
                $this->CI->db->order_by($this->order);
            }
            $offset = 0;
            if($this->page > 1) {
                $offset = ($this->page - 1) * $this->item_per_page;
            }
            $this->CI->db->limit($this->item_per_page, $offset);
            $data_return = $this->CI->db->get()->result_array();
            // Pagination Render
            $pagination = array(
                'total_data' => $data_count,
                'total_page' => ceil($data_count / $this->item_per_page),
                'item_per_page' => intval($this->item_per_page),
                'this_page' => intval($this->page)
            );
            $this->return['pagination'] = $pagination;
        } else { // No Pagination Show All
            $q_select = '';
            if (!empty($this->column)) {
                $count_col = COUNT($this->column);
                $cc = 1;
                foreach ($this->column as $k_col => $v_col) {
                    $comma = ', ';
                    if ($cc >= $count_col) {
                        $comma = '';
                    }
                    $q_select .= $k_col . ' AS ' . $v_col . $comma;
                    $cc++;
                }
            }
            $this->CI->db->select($q_select);
            $this->CI->db->from($this->table);
            // Join Table
            if (!empty($this->join_table) and !empty($this->join_param) and !empty($this->join_type)) {
                foreach ($this->join_table as $k_jt => $v_jt) {
                    $this->CI->db->join($this->join_table[$k_jt], $this->join_param[$k_jt], $this->join_type[$k_jt]);
                }
            }
            // Where Parameter
            if (!empty($this->where_column) and !empty($this->where_param)) {
                foreach ($this->where_column as $k_wc => $v_wc) {
                    $this->CI->db->where($this->where_column[$k_wc], $this->where_param[$k_wc]);
                }
            }
            // Where Parameter String
            if (!empty($this->where_string)) {
                foreach ($this->where_string as $v_ws) {
                    $this->CI->db->where($v_ws);
                }
            }
            // Where In
            if(!empty($this->where_in_column) AND !empty($this->where_in_param)) {
                $this->CI->db->where_in($this->where_in_column, $this->where_in_param);
            }
            // Ordering
            if (!empty($this->order)) {
                $this->CI->db->order_by($this->order);
            }
            // Limit
            if (!empty($this->limit_param)) {
                $this->CI->db->limit($this->limit_param);
            }
            $data_return = $this->CI->db->get()->result_array();
        }
        // To Many Relation
        if (!empty($this->to_many_table)) {
            foreach ($data_return as $k_dr => $v_dr) {
                // Query Relation To Many
                $this->CI->db->select();
                $this->CI->db->from($this->to_many_table);
                if (!empty($this->to_many_param)) {
                    foreach ($this->to_many_param as $k_tmp => $v_tmp) {
                        $this->CI->db->where($k_tmp, $v_dr[$v_tmp]);
                    }
                }
                if (!empty($this->to_many_join_table)) {
                    $this->CI->db->join($this->to_many_join_table, $this->to_many_join_param, $this->to_many_join_type);
                }
                $data_many = $this->CI->db->get()->result_array();
                $data_return[$k_dr][$this->to_many_key] = $data_many;
            }
        }
        $this->return['data'] = $data_return;
        return $this->return;
    }

}
