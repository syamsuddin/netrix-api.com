<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Zaincode {

    public function pass_hash($string = '') {
        return base64_encode(hash('sha256', sha1($string)));
    }
    
    function generateRandomString($length = 80) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function object_to_array($object = array()) {
        if (!empty($object)) {
            return json_decode(json_encode($object), TRUE);
        } else {
            return array();
        }
    }

    public function cdn_image($id = '', $type = '', $resolution = '') {
        return CONS_IMG_HOST . implode(str_split($id), '/') . '/' . $id . '_' . $resolution . '.' . $type;
    }

    public function security($post = array()) {
        $return = array();
        if (!empty($post)) {
            foreach ($post as $k_post => $v_post) {
                $post[$k_post] = $this->anti_injection($this->xss($this->bad_word_filter($v_post)));
            }
        }
        return $post;
    }

    public function dateformat_check($str = '') {
        if (!DateTime::createFromFormat('Y-m-d', $str)) { //yes it's YYYY-MM-DD
            return FALSE;
        } else {
            return TRUE;
        }
    }

    private function xss($val = '') {
        $val = preg_replace('/([\x00-\x08][\x0b-\x0c][\x0e-\x20])/', '', $val);
        $search = 'abcdefghijklmnopqrstuvwxyz';
        $search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $search .= '1234567890!@#$%^&*()';
        $search .= '~`";:?+/={}[]-_|\'\\';
        for ($i = 0; $i < strlen($search); $i++) {
            $val = preg_replace('/(&#[x|X]0{0,8}' . dechex(ord($search[$i])) . ';?)/i', $search[$i], $val); // with a ;
            $val = preg_replace('/(&#0{0,8}' . ord($search[$i]) . ';?)/', $search[$i], $val); // with a ;
        }
        $ra1 = Array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
        $ra2 = Array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
        $ra = array_merge($ra1, $ra2);
        $found = true;
        while ($found == true) {
            $val_before = $val;
            for ($i = 0; $i < sizeof($ra); $i++) {
                $pattern = '/';
                for ($j = 0; $j < strlen($ra[$i]); $j++) {
                    if ($j > 0) {
                        $pattern .= '(';
                        $pattern .= '(&#[x|X]0{0,8}([9][a][b]);?)?';
                        $pattern .= '|(&#0{0,8}([9][10][13]);?)?';
                        $pattern .= ')?';
                    }
                    $pattern .= $ra[$i][$j];
                }
                $pattern .= '/i';
                $replacement = substr($ra[$i], 0, 2) . '<x>' . substr($ra[$i], 2);
                $val = preg_replace($pattern, $replacement, $val);
                if ($val_before == $val) {
                    $found = false;
                }
            }
        }
        return $val;
    }

    private function anti_injection($data) {
        $filter = stripslashes(strip_tags(htmlspecialchars($data, ENT_QUOTES)));
        return $filter;
    }

    public function indonesian_date($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
        if (trim($timestamp) == '') {
            $timestamp = time();
        } elseif (!ctype_digit($timestamp)) {
            $timestamp = strtotime($timestamp);
        }
        # remove S (st,nd,rd,th) there are no such things in indonesia :p
        $date_format = preg_replace("/S/", "", $date_format);
        $pattern = array(
            '/Mon[^day]/', '/Tue[^sday]/', '/Wed[^nesday]/', '/Thu[^rsday]/',
            '/Fri[^day]/', '/Sat[^urday]/', '/Sun[^day]/', '/Monday/', '/Tuesday/',
            '/Wednesday/', '/Thursday/', '/Friday/', '/Saturday/', '/Sunday/',
            '/Jan[^uary]/', '/Feb[^ruary]/', '/Mar[^ch]/', '/Apr[^il]/', '/May/',
            '/Jun[^e]/', '/Jul[^y]/', '/Aug[^ust]/', '/Sep[^tember]/', '/Oct[^ober]/',
            '/Nov[^ember]/', '/Dec[^ember]/', '/January/', '/February/', '/March/',
            '/April/', '/June/', '/July/', '/August/', '/September/', '/October/',
            '/November/', '/December/',
        );
        $replace = array('Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Min',
            'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu',
            'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des',
            'Januari', 'Februari', 'Maret', 'April', 'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember',
        );
        $date = date($date_format, $timestamp);
        $date = preg_replace($pattern, $replace, $date);
        $date = "{$date} {$suffix}";
        return ucwords($date);
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'tahun',
            'm' => 'bulan',
            'w' => 'minggu',
            'd' => 'hari',
            'h' => 'jam',
            'i' => 'menit',
            's' => 'detik',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                //$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full)
            $string = array_slice($string, 0, 1);
        //return $string ? implode(', ', $string) . ' lalu' : 'sekarang';
        $return = '';
        $data_string = array();
        if (is_array($string)) {
            foreach ($string as $k_s => $v_s) {
                $data_string[] = $v_s;
            }
        }
        if (!empty($data_string[0])) {
            $return = $data_string[0] . ' lalu';
        } else {
            $return = 'sekarang';
        }
        return $return;
    }

    public function slugify($text) {
        $text = str_replace('\'', '', $text);
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        // trim
        $text = trim($text, '-');
        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);
        // lowercase
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }

    public function upload_image($file_s_files, $folder = '') {
        $datenow = date('YmdHis');
        $mime = array(
            'image/jpeg' => 'jpeg',
            'image/png' => 'png',
                //'application/x-shockwave-flash' => 'swf',
                //'image/psd' => 'psd',
                //'image/bmp' => 'bmp',
                //'image/tiff' => 'tiff',
                //'image/tiff' => 'tiff',
                //'image/jp2' => 'jp2',
                //'image/iff' => 'iff',
                //'image/vnd.wap.wbmp' => 'bmp',
                //'image/xbm' => 'xbm',
                //'image/vnd.microsoft.icon' => 'ico'
        );
        $upload_image_to_folder = UPLOAD_APPS_DIR . $folder . '/' . implode('/', str_split($datenow)) . '/';
        if (!file_exists(UPLOAD_APPS_DIR . $folder . '/' . implode('/', str_split($datenow)) . '/')) {
            mkdir(UPLOAD_APPS_DIR . $folder . '/' . implode('/', str_split($datenow)) . '/', 0777, true);
        }
        if (!empty($file_s_files)) {
            $file = $file_s_files;
            $file_name = $file['name'];
            $error = '';
            if (($file_s_files['size'] >= 2097152)) {
                $error .= 'File too large. File must be less than 2 megabytes. ';
            }
            $ext = strtolower(substr(strrchr($file_name, "."), 1));
            $file_info = getimagesize($file_s_files['tmp_name']);
            if (empty($file_info)) { // No Image?
                $error .= "The uploaded file doesn't seem to be an image.";
            } else { // An Image?
                $file_mime = $file_info['mime'];
                if ($ext == 'jpc' || $ext == 'jpx' || $ext == 'jb2') {
                    $extension = $ext;
                } else {
                    $extension = ($mime[$file_mime] == 'jpeg') ? 'jpg' : $mime[$file_mime];
                }
                if (!$extension) {
                    $extension = '';
                    $file_name = str_replace('.', '', $file_name);
                }
            }
            if ($error == "") {
                $new_file_name = $datenow . rand(1, 1000);
                $new_file_name .= '.' . $extension;
                $move_file = move_uploaded_file($file['tmp_name'], $upload_image_to_folder . $new_file_name);
                if ($move_file) {
                    @unlink($file['tmp_name']);
                    return array(
                        'url' => UPLOAD_APPS_DIR_HOST . $folder . '/' . implode('/', str_split($datenow)) . '/' . $new_file_name,
                        'path' => $upload_image_to_folder . $new_file_name
                    );
                }
            } else {
                @unlink($file['tmp_name']);
                return array(
                    'error' => $error
                );
            }
        } else {
            return array(
                'error' => 'image_file required'
            );
        }
    }

    public function upload_secure_image_user($user_id = '', $file_s_files) {
        $mime = array(
            'image/gif' => 'gif',
            'image/jpeg' => 'jpeg',
            'image/png' => 'png',
                //'application/x-shockwave-flash' => 'swf',
                //'image/psd' => 'psd',
                //'image/bmp' => 'bmp',
                //'image/tiff' => 'tiff',
                //'image/tiff' => 'tiff',
                //'image/jp2' => 'jp2',
                //'image/iff' => 'iff',
                //'image/vnd.wap.wbmp' => 'bmp',
                //'image/xbm' => 'xbm',
                //'image/vnd.microsoft.icon' => 'ico'
        );
        $upload_image_to_folder = CONS_USER_PATH . implode('/', str_split($user_id)) . '/';
        if (!file_exists(CONS_USER_PATH . implode('/', str_split($user_id)) . '/')) {
            mkdir(CONS_USER_PATH . implode('/', str_split($user_id)) . '/', 0777, true);
        }
        if (!empty($file_s_files)) {
            $file = $file_s_files['image_file'];
            $file_name = $file['name'];
            $error = '';
            if (($file_s_files['image_file']['size'] >= 2097152)) {
                $error .= 'File too large. File must be less than 2 megabytes. ';
            }
            $ext = strtolower(substr(strrchr($file_name, "."), 1));
            $file_info = getimagesize($file_s_files['image_file']['tmp_name']);
            if (empty($file_info)) { // No Image?
                $error .= "The uploaded file doesn't seem to be an image.";
            } else { // An Image?
                $file_mime = $file_info['mime'];
                if ($ext == 'jpc' || $ext == 'jpx' || $ext == 'jb2') {
                    $extension = $ext;
                } else {
                    $extension = ($mime[$file_mime] == 'jpeg') ? 'jpg' : $mime[$file_mime];
                }
                if (!$extension) {
                    $extension = '';
                    $file_name = str_replace('.', '', $file_name);
                }
            }
            if ($error == "") {
                $new_file_name = $user_id;
                $new_file_name .= '.' . $extension;
                $move_file = move_uploaded_file($file['tmp_name'], $upload_image_to_folder . $new_file_name);
                if ($move_file) {
                    @unlink($file['tmp_name']);
                    return array(
                        'url' => CONS_IMG_USER_HOST . implode('/', str_split($user_id)) . '/' . $new_file_name,
                        'path' => $upload_image_to_folder . $new_file_name
                    );
                }
            } else {
                @unlink($file['tmp_name']);
                return array(
                    'error' => $error
                );
            }
        } else {
            return array(
                'error' => 'image_file required'
            );
        }
    }

    public function upload_secure_image_selfie($uuid = '', $file_s_files) {
        $mime = array(
            'image/gif' => 'gif',
            'image/jpeg' => 'jpeg',
            'image/png' => 'png',
                //'application/x-shockwave-flash' => 'swf',
                //'image/psd' => 'psd',
                //'image/bmp' => 'bmp',
                //'image/tiff' => 'tiff',
                //'image/tiff' => 'tiff',
                //'image/jp2' => 'jp2',
                //'image/iff' => 'iff',
                //'image/vnd.wap.wbmp' => 'bmp',
                //'image/xbm' => 'xbm',
                //'image/vnd.microsoft.icon' => 'ico'
        );
        $upload_image_to_folder = CONS_ASSETS_PATH . '/selfie/' . implode('/', str_split($uuid)) . '/';
        if (!file_exists(CONS_ASSETS_PATH . '/selfie/' . implode('/', str_split($uuid)) . '/')) {
            mkdir(CONS_ASSETS_PATH . '/selfie/' . implode('/', str_split($uuid)) . '/', 0777, true);
        }
        if (!empty($file_s_files)) {
            $file = $file_s_files['image_file'];
            $file_name = $file['name'];
            $error = '';
            if (($file_s_files['image_file']['size'] >= 2097152)) {
                $error .= 'File too large. File must be less than 2 megabytes. ';
            }
            $ext = strtolower(substr(strrchr($file_name, "."), 1));
            $file_info = getimagesize($file_s_files['image_file']['tmp_name']);
            if (empty($file_info)) { // No Image?
                $error .= "The uploaded file doesn't seem to be an image.";
            } else { // An Image?
                $file_mime = $file_info['mime'];
                if ($ext == 'jpc' || $ext == 'jpx' || $ext == 'jb2') {
                    $extension = $ext;
                } else {
                    $extension = ($mime[$file_mime] == 'jpeg') ? 'jpg' : $mime[$file_mime];
                }
                if (!$extension) {
                    $extension = '';
                    $file_name = str_replace('.', '', $file_name);
                }
            }
            if ($error == "") {
                $new_file_name = date('YmdHis');
				$new_file_name_compress = $new_file_name . '-compress.' . $extension;
                $new_file_name .= '.' . $extension;
                $move_file = move_uploaded_file($file['tmp_name'], $upload_image_to_folder . $new_file_name);
				// Compress Image
				//$this->compress_image($file['tmp_name'], $upload_image_to_folder . $new_file_name_compress, 50);
                if ($move_file) {
                    @unlink($file['tmp_name']);
                    return array(
                        //'url' => CONS_ASSETS_URL . $new_file_name,
                        'url' => implode('/', str_split($uuid)) . '/' . $new_file_name,
                        'path' => $upload_image_to_folder . $new_file_name
                    );
                }
            } else {
                @unlink($file['tmp_name']);
                return array(
                    'error' => $error
                );
            }
        } else {
            return array(
                'error' => 'image_file required'
            );
        }
    }

	public function compress_image($source_url, $destination_url, $quality) {

      $info = getimagesize($source_url);

	  if ($info['mime'] == 'image/jpeg')
		$image = imagecreatefromjpeg($source_url);

	  elseif ($info['mime'] == 'image/gif')
		$image = imagecreatefromgif($source_url);

	  elseif ($info['mime'] == 'image/png')
		$image = imagecreatefrompng($source_url);

	  imagejpeg($image, $destination_url, $quality);
	  
	  return $destination_url;
		  
	}
	
    public function bad_word_filter($text = '') {
        $bad = array('4r5e', '5h1t', '5hit', 'a55', 'anal', 'anus', 'ar5e', 'arrse', 'arse', 'ass', 'ass-fucker', 'asses', 'assfucker', 'assfukka', 'asshole', 'assholes', 'asswhole', 'a_s_s', 'b!tch', 'b00bs', 'b17ch', 'b1tch', 'ballbag', 'balls', 'ballsack', 'bastard', 'beastial', 'beastiality', 'bellend', 'bestial', 'bestiality', 'bi+ch', 'biatch', 'bitch', 'bitcher', 'bitchers', 'bitches', 'bitchin', 'bitching', 'bloody', 'blow job', 'blowjob', 'blowjobs', 'boiolas', 'bollock', 'bollok', 'boner', 'boob', 'boobs', 'booobs', 'boooobs', 'booooobs', 'booooooobs', 'breasts', 'buceta', 'bugger', 'bum', 'bunny fucker', 'butt', 'butthole', 'buttmuch', 'buttplug', 'c0ck', 'c0cksucker', 'carpet muncher', 'cawk', 'chink', 'cipa', 'cl1t', 'clit', 'clitoris', 'clits', 'cnut', 'cock', 'cock-sucker', 'cockface', 'cockhead', 'cockmunch', 'cockmuncher', 'cocks', 'cocksuck', 'cocksucked', 'cocksucker', 'cocksucking', 'cocksucks', 'cocksuka', 'cocksukka', 'cok', 'cokmuncher', 'coksucka', 'coon', 'cox', 'crap', 'cum', 'cummer', 'cumming', 'cums', 'cumshot', 'cunilingus', 'cunillingus', 'cunnilingus', 'cunt', 'cuntlick', 'cuntlicker', 'cuntlicking', 'cunts', 'cyalis', 'cyberfuc', 'cyberfuck', 'cyberfucked', 'cyberfucker', 'cyberfuckers', 'cyberfucking', 'd1ck', 'damn', 'dick', 'dickhead', 'dildo', 'dildos', 'dink', 'dinks', 'dirsa', 'dlck', 'dog-fucker', 'doggin', 'dogging', 'donkeyribber', 'doosh', 'duche', 'dyke', 'ejaculate', 'ejaculated', 'ejaculates', 'ejaculating', 'ejaculatings', 'ejaculation', 'ejakulate', 'f u c k', 'f u c k e r', 'f4nny', 'fag', 'fagging', 'faggitt', 'faggot', 'faggs', 'fagot', 'fagots', 'fags', 'fanny', 'fannyflaps', 'fannyfucker', 'fanyy', 'fatass', 'fcuk', 'fcuker', 'fcuking', 'feck', 'fecker', 'felching', 'fellate', 'fellatio', 'fingerfuck', 'fingerfucked', 'fingerfucker', 'fingerfuckers', 'fingerfucking', 'fingerfucks', 'fistfuck', 'fistfucked', 'fistfucker', 'fistfuckers', 'fistfucking', 'fistfuckings', 'fistfucks', 'flange', 'fook', 'fooker', 'fuck', 'fucka', 'fucked', 'fucker', 'fuckers', 'fuckhead', 'fuckheads', 'fuckin', 'fucking', 'fuckings', 'fuckingshitmotherfucker', 'fuckme', 'fucks', 'fuckwhit', 'fuckwit', 'fudge packer', 'fudgepacker', 'fuk', 'fuker', 'fukker', 'fukkin', 'fuks', 'fukwhit', 'fukwit', 'fux', 'fux0r', 'f_u_c_k', 'gangbang', 'gangbanged', 'gangbangs', 'gaylord', 'gaysex', 'goatse', 'God', 'god-dam', 'god-damned', 'goddamn', 'goddamned', 'hardcoresex', 'hell', 'heshe', 'hoar', 'hoare', 'hoer', 'homo', 'horniest', 'horny', 'hotsex', 'jack-off', 'jackoff', 'jap', 'jerk-off', 'jism', 'jiz', 'jizm', 'jizz', 'kawk', 'knob', 'knobead', 'knobed', 'knobend', 'knobhead', 'knobjocky', 'knobjokey', 'kock', 'kondum', 'kondums', 'kum', 'kummer', 'kumming', 'kums', 'kunilingus', 'l3i+ch', 'l3itch', 'labia', 'lust', 'lusting', 'm0f0', 'm0fo', 'm45terbate', 'ma5terb8', 'ma5terbate', 'masochist', 'master-bate', 'masterb8', 'masterbat*', 'masterbat3', 'masterbate', 'masterbation', 'masterbations', 'masturbate', 'mo-fo', 'mof0', 'mofo', 'mothafuck', 'mothafucka', 'mothafuckas', 'mothafuckaz', 'mothafucked', 'mothafucker', 'mothafuckers', 'mothafuckin', 'mothafucking', 'mothafuckings', 'mothafucks', 'mother fucker', 'motherfuck', 'motherfucked', 'motherfucker', 'motherfuckers', 'motherfuckin', 'motherfucking', 'motherfuckings', 'motherfuckka', 'motherfucks', 'muff', 'mutha', 'muthafecker', 'muthafuckker', 'muther', 'mutherfucker', 'n1gga', 'n1gger', 'nazi', 'nigg3r', 'nigg4h', 'nigga', 'niggah', 'niggas', 'niggaz', 'nigger', 'niggers', 'nob', 'nob jokey', 'nobhead', 'nobjocky', 'nobjokey', 'numbnuts', 'nutsack', 'orgasim', 'orgasims', 'orgasm', 'orgasms', 'p0rn', 'pawn', 'pecker', 'penis', 'penisfucker', 'phonesex', 'phuck', 'phuk', 'phuked', 'phuking', 'phukked', 'phukking', 'phuks', 'phuq', 'pigfucker', 'pimpis', 'piss', 'pissed', 'pisser', 'pissers', 'pisses', 'pissflaps', 'pissin', 'pissing', 'pissoff', 'poop', 'porn', 'porno', 'pornography', 'pornos', 'prick', 'pricks', 'pron', 'pube', 'pusse', 'pussi', 'pussies', 'pussy', 'pussys', 'rectum', 'retard', 'rimjaw', 'rimming', 's hit', 's.o.b.', 'sadist', 'schlong', 'screwing', 'scroat', 'scrote', 'scrotum', 'semen', 'sex', 'sh!+', 'sh!t', 'sh1t', 'shag', 'shagger', 'shaggin', 'shagging', 'shemale', 'shi+', 'shit', 'shitdick', 'shite', 'shited', 'shitey', 'shitfuck', 'shitfull', 'shithead', 'shiting', 'shitings', 'shits', 'shitted', 'shitter', 'shitters', 'shitting', 'shittings', 'shitty', 'skank', 'slut', 'sluts', 'smegma', 'smut', 'snatch', 'son-of-a-bitch', 'spac', 'spunk', 's_h_i_t', 't1tt1e5', 't1tties', 'teets', 'teez', 'testical', 'testicle', 'tit', 'titfuck', 'tits', 'titt', 'tittie5', 'tittiefucker', 'titties', 'tittyfuck', 'tittywank', 'titwank', 'tosser', 'turd', 'tw4t', 'twat', 'twathead', 'twatty', 'twunt', 'twunter', 'v14gra', 'v1gra', 'vagina', 'viagra', 'vulva', 'w00se', 'wang', 'wank', 'wanker', 'wanky', 'whoar', 'whore', 'willies', 'willy', 'xrated', 'xxx', 'fuck', 'anjing', 'babi', 'monyet', 'kunyuk', 'bajingan', 'asu', 'bangsat', 'kampret', 'kontol', 'memek', 'ngentot', 'ngewe', 'ngehe', 'sepong', 'jembut', 'jembot', 'jemboot', 'jilmet', 'coli', 'onani', 'orgasme', 'doggy', 'titit', 'toket', 'boker', 'berak', 'perek', 'pecun', 'bencong', 'banci', 'jablay', 'maho', 'bispak', 'bego', 'goblok', 'goblog', 'idiot', 'geblek', 'orang gila', 'gila', 'sinting', 'tolol', 'sarap', 'udik', 'kampungan', 'kamseupay', 'sempak', 'bacot', 'meki', 'ngepet', 'oon', 'buta', 'budek', 'bolot', 'jelek', 'setan', 'iblis', 'keparat', 'bejad', 'gembel', 'brengsek', 'tai', 'tae', 'sompret', 'viagra', 'penis', 'vagina', 'obat kuat', 'sex', 'sex toys', 'seks');
        $replacements = "******";
        $ex = explode(" ", $text);
        $ex_fill = array();
        foreach ($ex as $v_ex) {
            if (in_array(strtolower($v_ex), $bad)) {
                $ex_fill[] = '******';
            } else {
                $ex_fill[] = $v_ex;
            }
        }
        return implode(" ", $ex_fill);
    }

    public function error_log($message = '') {
        log_message('error', $message);
        return $message;
    }

    public function base64_to_jpeg($base64_string = '', $user_id = '') {
        $output_file = CONS_USER_PATH . implode('/', str_split($user_id)) . '/' . $user_id . '.jpg';
        if (!file_exists(CONS_USER_PATH . implode('/', str_split($user_id)) . '/')) {
            mkdir(CONS_USER_PATH . implode('/', str_split($user_id)) . '/', 0777, true);
        }
        $ifp = fopen($output_file, 'wb');
        if (strpos($base64_string, ',') !== FALSE) {
            $explode = explode(',', $base64_string);
            if (!empty($explode[1])) {
                $base64_string = $explode[1];
            }
        }
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return CONS_IMG_USER_HOST . implode('/', str_split($user_id)) . '/' . $user_id . '.jpg';
    }

    public function base64_to_jpeg_selfie($base64_string = '', $uuid = '') {
        $filename = date('YmdHis');
        $output_file = CONS_ASSETS_PATH . '/selfie/' . implode('/', str_split($uuid)) . '/' . $filename . '.jpg';
        if (!file_exists(CONS_ASSETS_PATH . '/selfie/' . implode('/', str_split($uuid)) . '/')) {
            mkdir(CONS_ASSETS_PATH . '/selfie/' . implode('/', str_split($uuid)) . '/', 0777, true);
        }
        $ifp = fopen($output_file, 'wb');
        if (strpos($base64_string, ',') !== FALSE) {
            $explode = explode(',', $base64_string);
            if (!empty($explode[1])) {
                $base64_string = $explode[1];
            }
        }
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return implode('/', str_split($uuid)) . '/' . $filename . '.jpg';
    }

    public function curl_post($url = '', $request = '') {
        $data = array();
        if (!empty($url)) {
            $ch = curl_init($url);
            $options = array(
                CURLOPT_RETURNTRANSFER => true, // return web page
                CURLOPT_HEADER => false, // don't return headers
                CURLOPT_FOLLOWLOCATION => false, // follow redirects
                // CURLOPT_ENCODING       	=> "utf-8",           // handle all encodings
                CURLOPT_AUTOREFERER => true, // set referer on redirect
                CURLOPT_CONNECTTIMEOUT => 20, // timeout on connect
                CURLOPT_TIMEOUT => 20, // timeout on response
                CURLOPT_POST => 1, // i am sending post data
                //CURLOPT_HTTPHEADER => $headers,
                CURLOPT_POSTFIELDS => $request, // this are my post vars
                CURLOPT_SSL_VERIFYHOST => 0, // don't verify ssl
                CURLOPT_SSL_VERIFYPEER => false, //
                CURLOPT_VERBOSE => 1
            );
            curl_setopt_array($ch, $options);
            $data = curl_exec($ch);
            $curl_errno = curl_errno($ch);
            $curl_error = curl_error($ch);
            //echo 'error no : ' . $curl_errno;
            //echo 'error : ' . $curl_error;
            //print_r($data); exit();
            curl_close($ch);
        }
        return $data;
    }

    public function valid_secure_image($file_s_files) {
        $mime = array(
            'image/gif' => 'gif',
            'image/jpeg' => 'jpeg',
            'image/png' => 'png',
                //'application/x-shockwave-flash' => 'swf',
                //'image/psd' => 'psd',
                //'image/bmp' => 'bmp',
                //'image/tiff' => 'tiff',
                //'image/tiff' => 'tiff',
                //'image/jp2' => 'jp2',
                //'image/iff' => 'iff',
                //'image/vnd.wap.wbmp' => 'bmp',
                //'image/xbm' => 'xbm',
                //'image/vnd.microsoft.icon' => 'ico'
        );
        if (!empty($file_s_files)) {
            $file = $file_s_files['image_file'];
            $file_name = $file['name'];
            $error = '';
            if (($file_s_files['image_file']['size'] >= 2097152)) {
                $error .= 'File too large. File must be less than 2 megabytes. ';
            }
            $ext = strtolower(substr(strrchr($file_name, "."), 1));
            $file_info = getimagesize($file_s_files['image_file']['tmp_name']);
            if (empty($file_info)) { // No Image?
                $error .= "The uploaded file doesn't seem to be an image.";
            } else { // An Image?
                $file_mime = $file_info['mime'];
                if ($ext == 'jpc' || $ext == 'jpx' || $ext == 'jb2') {
                    $extension = $ext;
                } else {
                    $extension = ($mime[$file_mime] == 'jpeg') ? 'jpg' : $mime[$file_mime];
                }
                if (!$extension) {
                    $extension = '';
                    $file_name = str_replace('.', '', $file_name);
                }
            }
            if ($error == "") {
                return TRUE;
            } else {
                @unlink($file['tmp_name']);
                return array(
                    'error' => $error
                );
            }
        } else {
            return array(
                'error' => 'image_file required'
            );
        }
    }

    public function num_to_month($num = '') {
        $num = ltrim($num, '0');
        $arr_bulan = array(
            1 => 'JAN',
            2 => 'FEB',
            3 => 'MAR',
            4 => 'APR',
            5 => 'MEI',
            6 => 'JUN',
            7 => 'JUL',
            8 => 'AGU',
            9 => 'SEP',
            10 => 'OKT',
            11 => 'NOV',
            12 => 'DES'
        );
        return $arr_bulan[$num];
    }

}
